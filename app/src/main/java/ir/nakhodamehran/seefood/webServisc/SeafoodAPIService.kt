package ir.nakhodamehran.seefood.webServisc

import ir.nakhodamehran.seefood.models.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface SeafoodAPIService {
    @Headers("Content-Type: application/json")
    @POST("api/product")
    fun getProducts(@Body body:Map<String,String>):Call<MenuCategory>

    @Headers("Accept: application/json")
    @GET("api/getaddress")
    fun getAddress(@Header("Authorization") token:String):Call<List<Address>>

    @Headers("Content-Type: application/json")
    @POST("api/addaddress")
    fun addAddress(@Body body:Address,
                   @Header("Authorization") token:String):Call<Map<String,Int>>

    @Headers("Content-Type: application/json")
    @POST("api/editaddress")
    fun editAddress(@Body body:Address,
                   @Header("Authorization") token:String):Call<ResponseBody>


    @Headers("Content-Type: application/json")
    @GET("api/delete_address/{id}")
    fun deleteAddress(@Path("id") id:Int, @Header("Authorization") token:String):Call<ResponseBody>

    @GET("api/listRegion")
    fun getDistricts():Call<List<District>>

    @POST("api/statusProducts")
    fun checkStock(@Body body:Order,
                   @Header("Authorization")token:String):Call<ResponseBody>

    @POST("api/restaurant_status")
    fun getResturantStatus(@Body body:Map<String,String>):Call<ResturauntStatus>

    @Headers("Content-Type: application/json")
    @POST("api/login")
    fun loginUser(@Body body:Map<String,String>):Call<ResponseBody>


    @Headers("Content-Type: application/json")
    @POST("api/checkcode")
    fun getAuthToken(@Body body:Map<String,String>):Call<Map<String,Map<String,String>>>

    @GET("api/getprofile")
    fun getProfile(@Header("Authorization")token:String):Call<Map<String,Profile>>

    @GET("api/avatar")
    fun getAvatars():Call<List<Avatar>>


    @Headers("Content-Type: application/json")
    @POST("api/update_user")
    fun updateUser(@Header("Authorization")token:String,
        @Body body:Profile):Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("api/checkoff")
    fun setDiscount(@Header("Authorization")token:String,
                   @Body body:Map<String,String>):Call<Map<String,String>>


    @POST("api/addorder")
    fun addOrder(@Body body:Order,
                   @Header("Authorization")token:String):Call<Map<String,String>>

    @POST("api/get_ads")
    fun getAds(@Body body:Map<String,String>):Call<List<Map<String,String>>>

    @GET("api/last_orders_user")
    fun getFactor(@Header("Authorization")token:String):Call<List<Factor>>

    @Headers("Content-Type: application/json","Accept: application/json")
    @POST("api/get_messages")
    fun getMessages(@Header("Authorization")token:String,@Body body:Map<String,String>):Call<Map<String,List<Message>>>


    @POST("api/charge_wallet")
    fun chargeWallet(@Header("Authorization")token:String,
                     @Body body:Map<String,String>):Call<Map<String,String>>

    @POST("api/get_feedBack")
    fun sendFeedBack(@Header("Authorization")token:String,
                     @Body body:Map<String,String>):Call<ResponseBody>

    @POST("api/get_comments")
    fun getComments(@Header("Authorization")token:String,
                     @Body body:Map<String,String>):Call<List<Comment>>

    @POST("api/set_comment")
    fun setComments(@Header("Authorization")token:String,
                    @Body body:Map<String,String>):Call<ResponseBody>

    @POST("api/get_review")
    fun getReview(@Header("Authorization")token:String,
                    @Body body:Map<String,String>):Call<Review>

    @POST("api/get_about_us")
    fun getAboutMessage(@Body body:Map<String,String>):Call<Map<String,String>>
}