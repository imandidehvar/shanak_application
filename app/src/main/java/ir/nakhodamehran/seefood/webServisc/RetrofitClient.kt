package ir.nakhodamehran.seefood.webServisc

import ir.nakhodamehran.seefood.Utils.DataBindingAdapter
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

object RetrofitClient {

    val BASE_URL = "http://service.nakhodamehran.ir/"

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(DataBindingAdapter.getUnsafeOkHttpClient())
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val SEAFOOD_API_SERVICE: SeafoodAPIService= retrofit.create(SeafoodAPIService::class.java)


    fun parseError(response: Response<*>): Map<String,String> {
        val converter = retrofit
            .responseBodyConverter<Map<String,String>>(Map::class.java,arrayOfNulls<Annotation>(0))

        val error: Map<String,String>

        try {
            error = converter.convert(response.errorBody())!!
        } catch (e: IOException) {
            return mapOf()
        }

        return error
    }
}