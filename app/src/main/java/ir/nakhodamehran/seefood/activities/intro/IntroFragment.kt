package ir.nakhodamehran.seefood.activities.intro


import android.accounts.AccountManager
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.ADD_ACCOUNT
import ir.nakhodamehran.seefood.Utils.toggleVisibility
import ir.nakhodamehran.seefood.activities.Login.LoginActivity
import kotlinx.android.synthetic.main.fragment_intro.*


class IntroFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_intro, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        handlePages()
    }

    private fun handlePages(){
        val type=arguments?.getInt("introType")
        introDescription.typeface=ResourcesCompat.getFont(context!!,R.font.iran_sans_bold)

        enter_btn.setOnClickListener {
            val intent = Intent(activity, LoginActivity::class.java)
            intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, "ir.max_pc.seefood")
            intent.putExtra(ADD_ACCOUNT, true)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

        when(type){
          0->{
              var builder= SpannableString(getString(R.string.intro1))

              builder.setSpan(ForegroundColorSpan(Color.RED), builder.indexOf("۷"),
                  builder.indexOf("۰") + resources.getInteger(R.integer.introIndex), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
              introDescription.setText(builder , TextView.BufferType.SPANNABLE)
          }
            1->{
                introImage.setImageResource(R.drawable.ic_surprise)
                introDescription.setText(getString(R.string.intro2))
            }
            2->{
                introImage.setImageResource(R.drawable.ic_delivery)
                introDescription.setText(getString(R.string.intro3))
                enter_btn.toggleVisibility()
            }
        }
    }

}
