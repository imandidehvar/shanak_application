package ir.nakhodamehran.seefood.activities.Login

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.PHONE
import ir.nakhodamehran.seefood.Utils.isMobileNum
import ir.nakhodamehran.seefood.Utils.retrofitFailError
import ir.nakhodamehran.seefood.Utils.toggleVisibility
import ir.nakhodamehran.seefood.activities.SplashActivity
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.red_button.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


class LoginActivity : AppCompatActivity() {


    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        RequestSmsPermission()
        totalCounterText.toggleVisibility()
        confirmText.text=getString(R.string.next)
        confirm_btn.setOnClickListener {
            phoneInput.error=""

               if(phoneInput.editText?.text.isNullOrBlank())  {
                    phoneInput.error=getString(R.string.addPhoneErr)
                    return@setOnClickListener
                }
                if(!phoneInput.editText?.isMobileNum()!!|| phoneInput.editText?.text.toString()[0] !='0')  {
                    phoneInput.error=getString(R.string.formatPhoneErr)
                    return@setOnClickListener
                }

                    confirmText.toggleVisibility()
                    confirm_progress.toggleVisibility()
                    seafoodAPIService.loginUser(
                        mapOf("mobile" to phoneInput.editText?.text.toString().removeRange(0, 1)))
                        .enqueue(object : Callback<ResponseBody> {

                            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                phoneInput.retrofitFailError(t) {
                                    confirm_progress.toggleVisibility()
                                    confirmText.toggleVisibility()
                                    confirm_btn.performClick()
                                }
                            }

                            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                                confirmText.toggleVisibility()
                                confirm_progress.toggleVisibility()
                                if (response.isSuccessful) {
                                    val intent = Intent(baseContext, ConfirmCodeActivity::class.java)
                                    intent.putExtras(getIntent().extras!!)
                                    intent.putExtra(PHONE, phoneInput.editText?.text.toString())
                                    startActivity(intent)
                                }
                            }

                        })

            }
        }



    private fun RequestSmsPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS)
            != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.RECEIVE_SMS), SplashActivity.SMS_REQUESTCODE
            )
        }
    }



    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }


}
