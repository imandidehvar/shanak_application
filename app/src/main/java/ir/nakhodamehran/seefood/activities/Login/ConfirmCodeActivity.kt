package ir.nakhodamehran.seefood.activities.Login

import android.accounts.AccountAuthenticatorActivity
import android.accounts.AccountManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.CountDownTimer
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import com.github.florent37.viewanimator.ViewAnimator
import com.google.android.material.snackbar.Snackbar
import com.pixplicity.easyprefs.library.Prefs
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.activities.SplashActivity
import ir.nakhodamehran.seefood.activities.main.MainActivity
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import kotlinx.android.synthetic.main.activity_confirmation.*
import kotlinx.android.synthetic.main.red_button.*
import okhttp3.ResponseBody
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern


class ConfirmCodeActivity : AccountAuthenticatorActivity() {

    //this variable is just for creat custom account
    private lateinit var accountManager: AccountManager

    private var minuteCount=1
    private lateinit var secondTimer:CountDownTimer
    private var isClickable=true

    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)
        totalCounterText.toggleVisibility()
        confirmText.text=getString(R.string.next)
        configTimer()
        phone.text=intent.getStringExtra(PHONE)

        editPhone.setOnClickListener { onBackPressed() }

        getSmsReciver()

        confirm_btn.setOnClickListener {
            if(isClickable) {
                isClickable=false
                if (codeInput.text.isEmpty()) {
                    codeInput.error = getString(R.string.enterCode)
                    isClickable=true
                    return@setOnClickListener
                }
                confirm_progress.toggleVisibility()
                confirmText.toggleVisibility()

                seafoodAPIService.getAuthToken(
                    mapOf(
                        "mobile" to intent.getStringExtra(PHONE).removeRange(0,1),
                        "verifyCode" to codeInput.text.toString()
                    )
                )
                    .enqueue(object : Callback<Map<String, Map<String, String>>> {
                        override fun onFailure(call: Call<Map<String, Map<String, String>>>, t: Throwable) {
                            it.retrofitFailError(t) { confirm_btn.performClick() }
                            isClickable=true
                            confirm_progress.toggleVisibility()
                            confirmText.toggleVisibility()
                        }

                        override fun onResponse(
                            call: Call<Map<String, Map<String, String>>>,
                            response: Response<Map<String, Map<String, String>>>
                        ) {
                            isClickable=true
                            confirm_progress.toggleVisibility()
                            confirmText.toggleVisibility()
                            if (response.isSuccessful) {
                                val jobject = response.body()?.get("success")
                                val token = jobject?.get("token")
                               AppManager.authToken.set("Bearer $token")

                                    if (intent.getBooleanExtra(ADD_ACCOUNT, true))
                                        addAccount(token!!)
                                    else
                                        updateAccount(token!!)

                                if(response.code()==200)
                                    startActivity(intentFor<MainActivity>()
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
                                else if (response.code()==203){
                                    startActivity(intentFor<RegisterActivity>(PHONE to intent.getStringExtra(PHONE))
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
                                }

                            } else if (response.code() == 401) {
                                ViewAnimator.animate(codeInput).rubber().duration(350).start()
                                val snak =
                                    Snackbar.make(totalCounterText, getString(R.string.err_enter_code), Snackbar.LENGTH_LONG)
                                ViewCompat.setLayoutDirection(snak.view, ViewCompat.LAYOUT_DIRECTION_RTL)
                                snak.show()
                            }
                        }

                    })

            }
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
       if(requestCode== SplashActivity.SMS_REQUESTCODE && grantResults.isNotEmpty() &&
           grantResults[0] == PackageManager.PERMISSION_GRANTED){

           SmsReceiver.bindListener(object : SmsReceiver.SmsListener {
               override fun messageReceived(messageText: String) {
                   try {
                       val matcher = Pattern.compile(".*(\\d{4}).*").matcher(messageText)
                       if (matcher.find()) {
                           val code = matcher.group(1)
                           codeInput.setText(code)

                           confirm_btn.performClick()
                       }
                   } catch (e: Exception) {

                   }
               }
           })
       }
    }

    private fun updateAccount(token:String){
//        accountManager= AccountManager.get(baseContext)
//        val accounts = accountManager.getAccountsByType("ir.max_pc.seefood")
//        accountManager.setAuthToken(accounts[0],SeefoodAthenticator.TOKEN_TYPE,token)

        Prefs.putString(TOKEN,token)
    }


    private fun addAccount(token:String){
//        accountManager= AccountManager.get(baseContext)

//        val userName=intent.getStringExtra(PHONE)
        Prefs.putString(TOKEN,token)
//        val account = Account(name, intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE))
//        accountManager.addAccountExplicitly(account,"1234",null)
//        accountManager.setAuthToken(account,SeefoodAthenticator.TOKEN_TYPE,token)

//        val intent=intentFor<Any>(AccountManager.KEY_ACCOUNT_NAME to name,
//            AccountManager.KEY_ACCOUNT_TYPE to intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE),
//            AccountManager.KEY_AUTHTOKEN to token)

//        setAccountAuthenticatorResult(intent.extras)
//        setResult(Activity.RESULT_OK,intent)
    }




    private fun configTimer(){
//        TimeUnit.MINUTES.toMillis(minuteCount.toLong())
        minuteText.text="0$minuteCount"
        secondTimer=object : CountDownTimer(60000,1000) {
            override fun onFinish() {
               minuteCount--
                if (minuteCount!=-1){
                    secondTimer.start()
                    minuteText.setCounterAnimate("0$minuteCount")
                }
                else{
                    retryText.text=getString(R.string.resend)
                    secondText.toggleVisibility()
                    minuteText.toggleVisibility()
                    retryText.setOnClickListener{
                        if (isClickable) {
                            isClickable=false
                            retryCode()
                        }
                    }
                }
            }

            override fun onTick(time: Long) {
                secondText.setCounterAnimate(TimeUnit.MILLISECONDS.toSeconds(time).toString())
            }

        }
        secondTimer.start()
    }

    private fun retryCode(){
        seafoodAPIService.loginUser(mapOf("mobile" to intent.getStringExtra(PHONE).removeRange(0,1))).enqueue(object : Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                isClickable=true

            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                isClickable=true
                if(response.isSuccessful) {
                    toast(getString(R.string.sentCode))
                    minuteCount+=2
                    configTimer()
                    retryText.text=":"
                    retryText.setOnClickListener(null)
                    secondText.toggleVisibility()
                    minuteText.toggleVisibility()
                }
            }
        })
    }

    //get sms when user device receive sms
    private fun getSmsReciver(){
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS)
            != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                 arrayOf(android.Manifest.permission.RECEIVE_SMS), SplashActivity.SMS_REQUESTCODE)
        }
        else
            SmsReceiver.bindListener(object : SmsReceiver.SmsListener {
                override fun messageReceived(messageText: String) {
                        try {
                            val matcher = Pattern.compile(".*(\\d{4}).*").matcher(messageText)
                            if (matcher.find()) {
                                val code = matcher.group(1)
                                codeInput.setText(code)
                                confirm_btn.performClick()
                            }
                        } catch (e: Exception) {

                        }
                }
            })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == RESULT_OK &&  requestCode == REQ_REGISTER) {
            addAccount(data?.getStringExtra(AccountManager.KEY_AUTHTOKEN)!!)
        } else
            super.onActivityResult(requestCode, resultCode, data)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

}
