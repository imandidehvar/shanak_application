package ir.nakhodamehran.seefood.activities.Login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.activities.main.MainActivity
import ir.nakhodamehran.seefood.models.Profile
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import kotlinx.android.synthetic.main.activity_confirmation.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.red_button.*
import okhttp3.ResponseBody
import org.jetbrains.anko.intentFor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity : AppCompatActivity() {

    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        confirmText.text=getString(R.string.confirm)
        totalCounterText.toggleVisibility()
        confirm_btn.setOnClickListener {
            confirm_progress.toggleVisibility()
            confirmText.toggleVisibility()
            updateProfile()
        }
    }


    private fun updateProfile(){
        if(profileName.editText?.text!!.isEmpty()){
            profileName.error=getString(R.string.addNameErr)
            return
        }

        val profile= Profile(profileName.editText?.text.toString(),intent.getStringExtra(PHONE).removeRange(0,1),1,deviceId = getAndroidId(),inviteLink = "")
        seafoodAPIService.updateUser(AppManager.authToken.get()!!,profile).enqueue(object: Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                codeInput.retrofitFailError(t){
                    confirm_progress.toggleVisibility()
                    confirmText.toggleVisibility()
                    updateProfile()
                }
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if(response.isSuccessful)
                    startActivity(intentFor<MainActivity>()
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))

                confirm_progress.toggleVisibility()
                confirmText.toggleVisibility()

            }

        })
    }


}
