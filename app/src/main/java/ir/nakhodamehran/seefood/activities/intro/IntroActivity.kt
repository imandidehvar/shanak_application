package ir.nakhodamehran.seefood.activities.intro

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import ir.nakhodamehran.seefood.R
import kotlinx.android.synthetic.main.activity_intro.*
import org.jetbrains.anko.bundleOf
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class IntroActivity : AppCompatActivity() {

  val introList= listOf(IntroFragment(), IntroFragment(), IntroFragment())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)
        intro_viewpager.adapter=IntroAdapter(supportFragmentManager)

    }


    inner class IntroAdapter(fm:FragmentManager): FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            val intro=introList[position]
            intro.arguments= bundleOf("introType" to position)
            return intro
        }

        override fun getCount(): Int = introList.size


    }
    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }
}
