package ir.nakhodamehran.seefood.activities.main

import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel
import com.pixplicity.easyprefs.library.Prefs
import ir.nakhodamehran.seefood.BuildConfig
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.AppManager
import ir.nakhodamehran.seefood.Utils.LANGUAGE
import ir.nakhodamehran.seefood.Utils.retrofitFailError
import ir.nakhodamehran.seefood.models.District
import ir.nakhodamehran.seefood.models.MenuCategory
import ir.nakhodamehran.seefood.models.Profile
import ir.nakhodamehran.seefood.models.ResturauntStatus
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import okhttp3.ResponseBody
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainViewModel : ViewModel(){
    val hasExpierdToken=ObservableBoolean(true)
    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }


     fun getMenuCategory(view: View, language:String, callBack:(Response<*>)->Unit,retry:()->Unit){
    seafoodAPIService.getProducts(mapOf("language" to language)).enqueue(object : Callback<MenuCategory> {
        override fun onResponse(call: Call<MenuCategory>, response: Response<MenuCategory>) {
            if(response.isSuccessful) {
                val menuCategory=response.body()
                //for spiting Custom Description from FastDescription
                menuCategory?.mainList?.map{
                    val descriptions=it.description.split(",")
                    it.coustomDescription=descriptions[0]
                    it.description=descriptions[1]
                }
                AppManager.menuCategory=menuCategory
                callBack(response)
            } else if(response.code()==500)
                view.context.toast(view.context.getString(R.string.err_support_call))
        }

        override fun onFailure(call: Call<MenuCategory>, t: Throwable) {
            view.retrofitFailError(t){retry()}
        }
    })
    }


    fun getResturantStatus(showAppUpdateDialog:()->Unit,showNoService:(String)->Unit){
        seafoodAPIService.getResturantStatus(mapOf("lang" to Prefs.getString(LANGUAGE,"fa"))).enqueue(object :Callback<ResturauntStatus>{
            override fun onFailure(call: Call<ResturauntStatus>, t: Throwable) {
            }

            override fun onResponse(call: Call<ResturauntStatus>, response: Response<ResturauntStatus>) {
                if(response.isSuccessful){
                    AppManager.walletGiftPrice=response.body()?.walletCharge
                    if(BuildConfig.VERSION_CODE<response.body()?.version!!.toInt())
                        showAppUpdateDialog()
                    if(response.body()?.status=="0")
                        showNoService(response.body()!!.message)
                }
            }

        })
    }

    fun getDistiricts(view: View,callBack:(Response<*>)->Unit,retry:()->Unit){
        seafoodAPIService.getDistricts().enqueue(object : Callback<List<District>>{
            override fun onResponse(call: Call<List<District>>, response: Response<List<District>>) {
                if(response.isSuccessful) {
                 AppManager.districts=response.body()
                    callBack(response)
                } else if(response.code()==500)
                    view.context.toast(view.context.getString(R.string.err_support_call))
            }

            override fun onFailure(call: Call<List<District>>, t: Throwable) {
                view.retrofitFailError(t){retry()}
            }
        })
    }

    fun getProfile(view: View,isTokenRefresh:(Boolean)->Unit,retry:()->Unit){
        seafoodAPIService.getProfile(AppManager.authToken.get()!!).enqueue(object : Callback<Map<String,Profile>>{

            override fun onResponse(call: Call<Map<String, Profile>>, response: Response<Map<String, Profile>>) {
                if(response.isSuccessful) {
                   AppManager.profile = response.body()?.get("success")
                    hasExpierdToken.set(false)
                    isTokenRefresh(false)
                }else if(response.code()==406)
                   isTokenRefresh(true)

            }

            override fun onFailure(call: Call<Map<String, Profile>>, t: Throwable) {
                view.retrofitFailError(t){ retry()}
            }
        })
    }

    fun updateProfile(view: View, profile:Profile){
        seafoodAPIService.updateUser(AppManager.authToken.get()!!,profile).enqueue(object: Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

            }

        })
    }
}
