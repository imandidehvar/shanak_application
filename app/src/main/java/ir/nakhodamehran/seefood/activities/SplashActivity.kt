package ir.nakhodamehran.seefood.activities

import android.accounts.AccountManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.Observable
import androidx.lifecycle.ViewModelProviders
import com.github.florent37.viewanimator.ViewAnimator
import com.google.firebase.analytics.FirebaseAnalytics
import com.pixplicity.easyprefs.library.Prefs
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.activities.Login.ConfirmCodeActivity
import ir.nakhodamehran.seefood.activities.Login.LoginActivity
import ir.nakhodamehran.seefood.activities.intro.IntroActivity
import ir.nakhodamehran.seefood.activities.main.MainActivity
import ir.nakhodamehran.seefood.activities.main.MainViewModel
import kotlinx.android.synthetic.main.activity_splash.*
import org.jetbrains.anko.intentFor
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


class SplashActivity : AppCompatActivity(),View.OnClickListener {

    companion object {
        var SMS_REQUESTCODE=100
    }
    private val viewModel: MainViewModel by lazy {
        ViewModelProviders.of(this).get(MainViewModel::class.java)
    }

    private val fcmAnalitics:FirebaseAnalytics by lazy {
        FirebaseAnalytics.getInstance(this)
    }

    private lateinit var accountManager:AccountManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
      StatusBarUtils.immersive(this)
        setContentView(R.layout.activity_splash)

        val language= Prefs.getString(LANGUAGE,"fa")
        //check app open for first time in whole
        val isFirst= Prefs.getBoolean(FIRST,true)

        perButton.setOnClickListener(this)
        arabButton.setOnClickListener(this)
        when {
            language=="ar" -> changeLanguage(language)
            isFirst -> {
                perButton.toggleVisibility()
                arabButton.toggleVisibility()
            }
        }

        if(!isFirst)
            getMenuCategory(language)



        logo.afterMeasured {
            ViewAnimator.animate(this).fadeIn().duration(1000).start()

            if (isFirst) {
                ViewAnimator.animate(perButton).fadeIn().duration(1000).start()
                ViewAnimator.animate(arabButton).fadeIn().duration(1000).start()
            }
        }

        arabButton.setOnClickListener(this)
    }


    private fun getAccount(){
//        accountManager= AccountManager.get(baseContext)
//        val accounts = accountManager.getAccountsByType("ir.max_pc.seefood")
        if (Prefs.getString(TOKEN,"").isEmpty()) {
            //show Intro fo the First
            startActivity(intentFor<IntroActivity>()
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
        }
        else {
//            getProfile(accounts[0])
            getProfile()
            viewModel.hasExpierdToken.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback(){
                override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                    getDistrict()
                }

            })
        }

    }

    private fun getProfile(){
//        object: Thread(){
//            override fun run() {
//                AppManager.authToken.set("Bearer "+accountManager.blockingGetAuthToken(account,SeefoodAthenticator.TOKEN_TYPE,true))
//            }
//        }.start()
        AppManager.authToken.addOnPropertyChangedCallback(object: Observable.OnPropertyChangedCallback(){
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                viewModel.getProfile(logo,{isTokenRefresh->
                    if(isTokenRefresh) {
                        //when token is invalid below code invalidate the  token
//                        accountManager.invalidateAuthToken(SeefoodAthenticator.TOKEN_TYPE, AppManager.authToken.get())

                        startAccountActivityDirectly(false)
                    }
                }){
//                    getProfile(account)
                    getProfile()
                }
            }
        })

        AppManager.authToken.set("Bearer "+Prefs.getString(TOKEN,""))

    }

    private fun startAccountActivityDirectly(boolean: Boolean) {
        val intent = Intent(this, LoginActivity::class.java)
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, "ir.max_pc.seefood")
        intent.putExtra(ADD_ACCOUNT, boolean)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)

    }

    override fun onClick(view: View?) {
       when (view?.id) {
           R.id.arabButton->{
               showProgress()
               changeLanguage("ar")
               Prefs.putString(LANGUAGE,"ar")
              getMenuCategory("ar")
               fcmAnalitics.setUserProperty("user_type","user_ar")
           }

           else->{
               showProgress()
               getMenuCategory("fa")
               fcmAnalitics.setUserProperty("user_type","user_fa")

           }
       }
        Prefs.putBoolean(FIRST,false)
    }

    private fun getMenuCategory(lang:String){
        viewModel.getMenuCategory(logo,lang,{

            getAccount()
        }){ getMenuCategory(lang)}
    }

    private fun getDistrict(){
        viewModel.getDistiricts(logo,{
            Handler().postDelayed({
                startActivity(intentFor<MainActivity>()
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
            },1000)

        }){getDistrict()}
    }

    private fun showProgress(){
        ViewAnimator.animate(perButton).dp().translationX(0f,50f).onStop {
            ViewAnimator.animate(perButton).fadeOut().duration(100).start()
            ViewAnimator.animate(progress).fadeIn().duration(100).start()
        }.duration(250).start()
        ViewAnimator.animate(arabButton).dp().translationX(0f,-30f).onStop {
            ViewAnimator.animate(arabButton).fadeOut().duration(100).start()
        }.duration(250).start()
    }



    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }


}
