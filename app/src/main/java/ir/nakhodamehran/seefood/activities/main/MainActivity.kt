package ir.nakhodamehran.seefood.activities.main

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import co.ronash.pushe.Pushe
import com.pixplicity.easyprefs.library.Prefs
import ir.nakhodamehran.seefood.BuildConfig
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.databinding.ActivityMainBinding
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.drawer_header.view.*
import kotlinx.android.synthetic.main.error_dialog.*
import kotlinx.android.synthetic.main.wallet_price.view.*
import org.jetbrains.anko.toast
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    private lateinit var header: View

    //initilazng MainViewModel
    private val viewModel: MainViewModel by lazy {
        ViewModelProviders.of(this).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        StatusBarUtils.immersive(this)


        resources.configuration.smallestScreenWidthDp

        binding = DataBindingUtil.setContentView(this@MainActivity, R.layout.activity_main)


        navConfig()
        binding.content.pageTitle.typeface = ResourcesCompat.getFont(this, R.font.iran_sans_bold)
        binding.content.backButton.setOnClickListener { onBackPressed() }
        StatusBarUtils.setPaddingSmart(this, binding.content.toolbar)
        setProfile()
        getResturantStatus()
    }


    /***
     * this check account is valid or not
     */
    override fun onStart() {
        super.onStart()
        checkAccount()
    }


    var doubleBackToExitPressedOnce = false

    override fun onBackPressed() {
        if (nav_host.findNavController().currentDestination?.id == R.id.mainFragment) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed()
                return
            }

            this.doubleBackToExitPressedOnce = true
            toast(getString(R.string.exitApp))

            Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)

        } else
            super.onBackPressed()
    }

    //get resturant info message for closed time,wallet giftPrice , opening status ,check for update version
    private fun getResturantStatus() {
        viewModel.getResturantStatus({

            val builder = AlertDialog.Builder(this)
            builder.setView(R.layout.error_dialog)
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.show()
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.title.text = resources.getString(R.string.support)
            dialog.message.text = getString(R.string.appUpdateMessage)
            dialog.positiveButton.text = resources.getString(R.string.okey)
            dialog.positiveButton.setOnClickListener {

                var url = "https://cafebazaar.ir/app/ir.nakhodamehran.seefood/?l=fa"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
            dialog.negativeButton.setOnClickListener { dialog.dismiss() }
        }) {

            val builder = AlertDialog.Builder(this)
            builder.setView(R.layout.error_dialog)
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.show()
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.negativeButton.toggleVisibility()
            dialog.title.text = resources.getString(R.string.support)
            dialog.message.text = it
            dialog.positiveButton.text = resources.getString(R.string.okey)
            dialog.positiveButton.setOnClickListener { dialog.dismiss() }
        }
    }


    //region Navigation

    private fun navConfig() {
        val toggle = ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            binding.content.toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )


        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        binding.navView.itemIconTintList = null
        //config navigation Architecture for navigation drawer
        binding.navView.setupWithNavController(
            nav_host.findNavController()
        )

        binding.navView.getChildAt(0)?.isVerticalScrollBarEnabled = false
    }
    //endregion

    // set data in drawer item's action view
    private fun setWalletPrice() {
        val menu = binding.navView.menu
        val textView = menu.findItem(R.id.walletFragment).actionView.price
        //wallet item
        if (Prefs.getString(LANGUAGE, "fa") == "fa")
            textView.text = "اعتبار فعلی :${AppManager.profile?.wallet} تومان"
        else
            textView.text = "رصيدك الحالي : ${AppManager.profile?.wallet}تومان"

        //set color for invite item
        val inviteItem = menu.findItem(R.id.inviteFragment)
        val spanString = SpannableString(inviteItem.title)
        spanString.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)),
            0,
            spanString.length,
            0
        )
        inviteItem.title = spanString

        //software version
        val versionText = menu.findItem(R.id.version).actionView.price
        versionText.text = BuildConfig.VERSION_NAME
    }

    //this is function to set profile to drawer header
    private fun setProfile() {
        header = binding.navView.getHeaderView(0)
        if (AppManager.profile != null) {
            setWalletPrice()

            header.profileName.text = AppManager.profile?.name
            header.profilePhone.text = AppManager.profile?.mobile

            displayCercularImage(RetrofitClient.BASE_URL + AppManager.profile?.avatarImage, header.profileImage)
        } else {
            getProfile()
            getDistrict()
            getMenucategory()
        }

        header.setOnClickListener {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
            nav_host.findNavController().popBackStack(R.id.mainFragment, false)
            nav_host.findNavController().navigate(R.id.action_to_profileFragment)
        }

        //this for test menu fragment
//        header.profileImage.setOnLongClickListener{
//            nav_host.findNavController().navigate(R.id.action_to_menuFragment)
//            true
//        }
    }


    fun getProfile() {

        viewModel.getProfile(binding.navView, {
            if (!it) {
                setWalletPrice()
                header.profileName.text = AppManager.profile?.name
                header.profilePhone.text = AppManager.profile?.mobile
                displayCercularImage(RetrofitClient.BASE_URL + AppManager.profile?.avatarImage, header.profileImage)
                //updateProfile for deviceId
                AppManager.profile?.deviceId = Pushe.getPusheId(this)
                viewModel.updateProfile(binding.navView, AppManager.profile!!)
            }
        }, {
            getProfile()
        })
    }


    private fun getDistrict() {
        viewModel.getDistiricts(binding.navView, {}) {
            getDistrict()
        }
    }


    private fun getMenucategory() {
        viewModel.getMenuCategory(binding.navView, Prefs.getString(LANGUAGE, "fa"), {}) {
            getDistrict()
        }
    }

    override fun onSupportNavigateUp() =
        findNavController(this, R.id.nav_host).navigateUp()


    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }


}


