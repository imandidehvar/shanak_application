package ir.nakhodamehran.seefood.models

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Profile(var name:String,
                   var mobile:String,
                   var avatarId:Int,
                   var wallet:Int?=null,
                   @SerializedName("avatar") var avatarImage:String?=null,
                   var deviceId:String="web",
                   @SerializedName("guId") var inviteLink:String)


@Keep
data class Avatar(val id:Int,
                  val title:String,
                  @SerializedName("url") val image:String)


@Keep
data class Message(val date:String,
                   val title:String,
                   val message:String)

data class ResturauntStatus(val status:String,
                            val message:String,
                            val version:String="-1",
                            val walletCharge: String)