package ir.nakhodamehran.seefood.models

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
class Factor(val id:Int
             , val date:String
             , val orderItems:String
             , val order_price:String
             , val deliveryPrice:String
             , val offPrice:String
             , val payPrice:String
             , var payway:String
             , val factorAddress:String
             , val url:String?
             , val status_message:String
             , val status:String): Parcelable