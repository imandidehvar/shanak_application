package ir.nakhodamehran.seefood.models

class Comment(var id:Int
              ,var date:String
              ,var rate:Int
              ,var message:String
              ,var avatar:String
              ,var name:String)

class Review(var text1:String,
             var text2:String,
             var text3:String,
             var title:String,
             var img1:String,
             var video:String,
             var img2:String)