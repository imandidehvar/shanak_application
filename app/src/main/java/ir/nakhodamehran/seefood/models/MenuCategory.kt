package ir.nakhodamehran.seefood.models

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class MenuCategory(
    @SerializedName("main") val mainList: List<Main>? = null,
    @SerializedName("side") val sideList: List<Side>? = null,
    @SerializedName("dessert") val dessertList: List<Dessert>? = null,
    @SerializedName("drink") val drinkList: List<Drink>? = null
)

@Keep
data class Main(val id: Int, val title: String, val price: String, val image: String,
                val fastImage: String,var coustomDescription: String="",val offProduct:String,
                val icon:String, var description: String = "",var rate:Int=0,var time:Int=0)
@Keep
data class Side(val id: Int, val title: String, val image: String,val offProduct:String,val icon:String, val description: String = "")
@Keep
data class Dessert(val id: Int, val title: String, val price: String, val image: String,val offProduct:String, val description: String = "")
@Keep
data class Drink(val id: Int, val title: String, val price: String,val offProduct:String, val image: String,val description: String = "")

/***
 * isCountered= that's for when user  countered the food or drinks and etc showing counterBox in menuListItem
 * timerRunnable must be Any type because Gson could not convert Runnable  type to json
 */
@Keep
open class ListMenu(
    var id: Int=-1,
    var title: String? = "",
    var description: String? = "",
    var timerRunnable: Any? = null,
    var price: String? = "",
    var offProduct: String="",
    var rate: Int=0,
    var time: Int=0,
    var image: String? = "",
    var count: Int = 0,
    var sideList: List<Int> = listOf(),
    var isCategoryName: Boolean? = false,
    var isCountered: Boolean? = false,
    var isDessert:Boolean?=false
)
