package ir.nakhodamehran.seefood.models

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Keep
data class Address(@SerializedName("id") var Id:Int=-1,
                   var regionId:Int=-1,
                   var title:String="",
                   var phone:String="",
                   var address:String=""): Parcelable

@Keep
data class District(@SerializedName("id") var Id:Int=-1,
                    var title:String="",var price:String="")