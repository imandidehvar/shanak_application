package ir.nakhodamehran.seefood.models

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Order(
    @SerializedName("dish") var dishes: MutableSet<DishOrder> = mutableSetOf(),
    @SerializedName("dessert") var desserts: MutableList<ListMenu> = mutableListOf(),
    @SerializedName("drink") var drinks: MutableList<ListMenu> = mutableListOf(),
    var address:Int = -1,
    var useWallet:Int = 0,
    var update:Int? = null,
    var language:String?=null,
    var discount:String="",
    var description:String="",
    var payWay:String="online"
)


/***
 * count=for count of DishOrder that user order that
 * mainID= id of Main Food that user urder.
 * side=is decoration of food like tomatoes;
 */
@Keep
data class DishOrder(
    var count: Int = 0,
    @SerializedName("id") var mainID: Int = -1,
    var price: String = "0",
    var side: MutableList<Int> = mutableListOf()
)

@Keep
data class Dish(var main: Int = 0, var side: MutableList<Side> = mutableListOf())


