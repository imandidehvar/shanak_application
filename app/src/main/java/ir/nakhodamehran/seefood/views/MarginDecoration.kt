package ir.nakhodamehran.seefood.views

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.dpToPx

class MarginDecoration(val isMenu:Boolean): RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View,
                                parent: RecyclerView, state: RecyclerView.State) {
        with(outRect){
            if(parent.getChildAdapterPosition(view)==0)
                right = if(!isMenu)
                    parent.resources.dpToPx(parent.resources.getInteger(R.integer.dish_margin).toFloat())
                else
                    parent.resources.dpToPx(parent.resources.getInteger(R.integer.menu_margin).toFloat())


            if(isMenu){
                if(parent.getChildAdapterPosition(view)==state.itemCount-1)
                    left=parent.resources.dpToPx(parent.resources.getInteger(R.integer.menu_margin).toFloat())
            }
//            if(parent.getChildAdapterPosition(view)==9)
//                left=view1.right/6
//            when{
//                parent.getChildAdapterPosition(view)==0->right=view1.right/6
//                parent.getChildAdapterPosition(view)==9->{right=space
//                left=space*2}
//                else->right=view1.right/6
//            }


        }

    }
}