package ir.nakhodamehran.seefood.Utils;

import android.widget.ImageView;
import android.widget.TextView;
import androidx.databinding.BindingAdapter;
import com.google.gson.Gson;
import ir.nakhodamehran.seefood.R;
import ir.nakhodamehran.seefood.models.*;
import ir.nakhodamehran.seefood.webServisc.RetrofitClient;
import okhttp3.OkHttpClient;

import javax.net.ssl.*;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

public class DataBindingAdapter {
    @BindingAdapter("imageResource")
    public static void setImageResours(final ImageView view, final String url) {

        GlideApp
                .with(view.getContext())
                .load(RetrofitClient.INSTANCE.getBASE_URL() + url)
                .placeholder(R.drawable.ic_fish_icon) // can also be a drawable
                .into(view);
    }


    @BindingAdapter("setPrice")
    public static void setpriceForText(TextView view, String price) {
        String result;
        try {
            result = String.format("%,d", Long.parseLong(price)) + " تومان";
            view.setText(result);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }


    @BindingAdapter("setCount")
    public static void setCounter(TextView view, String counter) {
        String result = null;
        try {
            result = counter;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        ExtentionsKt.setCounterAnimate(view, result);
    }


    @BindingAdapter("setDate")
    public static void setDate(TextView view, String date) {
        try {
            String[] result = date.split(" ");
            view.setText(result[0]);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    @BindingAdapter("setTime")
    public static void setTime(TextView view, String time) {
        try {
            String[] totalTime = time.split(" ");
            String[] result=totalTime[1].split(":");
            view.setText(view.getContext().getString(R.string.clock)+result[0]+":"+result[1]);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
    @BindingAdapter("showOrders")
    public static void showOrder(TextView view, String orders) {
        StringBuilder result = new StringBuilder();

        Order selectedOrder = new Gson().fromJson(orders, Order.class);
        try {
            for (DishOrder orderedMain : selectedOrder.getDishes())
                for (Main main : AppManager.Companion.getMenuCategory().getMainList())
                    if (main.getId() == orderedMain.getMainID()) {
                        result.append(orderedMain.getCount()).append(" عدد ");
                        result.append(main.getTitle()).append(" , ");
                        break;
                    }

            if (selectedOrder.getDesserts().size()!=0 || selectedOrder.getDrinks().size()!=0)
                result.append(view.getContext().getString(R.string.with));
            else
                result.deleteCharAt(result.length()-2);

            for (ListMenu orderedDessert : selectedOrder.getDesserts())
                for (Dessert dessert : AppManager.Companion.getMenuCategory().getDessertList())
                    if (dessert.getId() == orderedDessert.getId()) {
                        result.append(orderedDessert.getCount()).append(" عدد ");
                        result.append(dessert.getTitle()).append(" ");
                        break;
                    }

            for (ListMenu orderedDrinks : selectedOrder.getDrinks())
                for (Drink drink : AppManager.Companion.getMenuCategory().getDrinkList())
                    if (drink.getId() == orderedDrinks.getId()) {
                        result.append(orderedDrinks.getCount()).append(" عدد ");
                        result.append(drink.getTitle()).append(" ");
                        break;
                    }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        view.setText(result);
    }

    @BindingAdapter("showStatus")
    public static void showStatus(TextView view, String status) {
     switch (status){
         case "onPrepare":
             view.setText(view.getContext().getString(R.string.onPrepare));
             break;
         case "onDeliver":
             view.setText(view.getContext().getString(R.string.onDeliver));
             break;
         case "onConfirm":
             view.setText(view.getContext().getString(R.string.onConfirm));
             break;
         case "onPay":
             view.setText(view.getContext().getString(R.string.onPay));
             break;
         case "onCancel":
             view.setText(view.getContext().getString(R.string.onCancel));
             break;
         case "onComplete":
             view.setText(view.getContext().getString(R.string.onComplete));
             break;

     }
    }

    public static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.readTimeout(18, TimeUnit.SECONDS);
            builder.connectTimeout(18, TimeUnit.SECONDS);
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

