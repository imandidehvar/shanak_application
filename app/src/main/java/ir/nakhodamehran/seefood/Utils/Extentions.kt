package ir.nakhodamehran.seefood.Utils

import android.accounts.AccountManager
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Bitmap
import android.provider.Settings
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.animation.LinearInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.core.view.ViewCompat
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.pixplicity.easyprefs.library.Prefs
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.activities.Login.LoginActivity
import ir.nakhodamehran.seefood.activities.SplashActivity
import java.io.IOException
import java.net.SocketTimeoutException
import java.util.*
import java.util.regex.Pattern


inline fun <T : View> T.afterMeasured(crossinline f: T.() -> Unit) {
    viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            if (measuredWidth > 0 && measuredHeight > 0) {
                viewTreeObserver.removeOnGlobalLayoutListener(this)
                f()
            }
        }
    })
}

fun Resources.dpToPx(dp: Float): Int {
    val px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, this.displayMetrics)
    return px.toInt()

}

fun View.toggleVisibility(): View {
    visibility = if (visibility == View.VISIBLE) {
        View.GONE
    } else {
        View.VISIBLE
    }
    return this
}

fun EditText.isMobileNum(): Boolean {
    val sPattern = Pattern.compile("(\\+98|0)?9\\d{9}")
    return sPattern.matcher(this.text.toString()).matches()
}

fun Context.changeLanguage(language: String) {
    val locale = Locale(language)
    Locale.setDefault(locale)
    val config = Configuration()
    config.locale = locale
    this.resources.updateConfiguration(
        config,
        this.resources.displayMetrics
    )
}

fun Context.restartApp() {
    val intent = Intent(this, SplashActivity::class.java)
    val mPendingIntentId = 1
    val mPendingIntent = PendingIntent.getActivity(this, mPendingIntentId, intent, PendingIntent.FLAG_CANCEL_CURRENT)
    val mgr = this.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent)
    System.exit(0)
}

fun View.retrofitFailError(t: Throwable, tryAgain: () -> Unit) {
    val snackBar = when (t) {
        is IOException -> {
            Snackbar
                .make(this, context.getString(R.string.check_connection), Snackbar.LENGTH_INDEFINITE)
                .setAction(context.getString(R.string.retry)) { tryAgain() }
        }
        is SocketTimeoutException -> {
            Snackbar
                .make(this, context.getString(R.string.slowNetwork), Snackbar.LENGTH_INDEFINITE)
                .setAction(context.getString(R.string.retry)) { tryAgain() }
        }
        else -> Snackbar
            .make(this, context.getString(R.string.err_support_call), Snackbar.LENGTH_INDEFINITE)
            .setAction(context.getString(R.string.retry)) { tryAgain() }
    }

    ViewCompat.setLayoutDirection(snackBar.view, ViewCompat.LAYOUT_DIRECTION_RTL)
    snackBar.setActionTextColor(ContextCompat.getColor(this.context,android.R.color.holo_orange_light))
    snackBar.show()
}


fun TextView.setCounterAnimate( value: String) {
    this.animate().scaleY(0f).setInterpolator(LinearInterpolator()).setDuration(200).setListener(object :
        AnimatorListenerAdapter() {
        override fun onAnimationEnd(animation: Animator) {
            this@setCounterAnimate.text = value
            this@setCounterAnimate.animate().scaleY(1f).setInterpolator(LinearInterpolator()).setDuration(200).setListener(object :
                AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator, isReverse: Boolean) {
                }
            }).start()
        }
    }).start()

}

fun Context.displayCercularImage(url:String,imageView: ImageView){
    GlideApp.with(this).asBitmap().load(url).centerCrop().into(object : BitmapImageViewTarget(imageView) {
        override fun setResource(resource: Bitmap?) {
            val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(resources, resource)
            circularBitmapDrawable.setCircular(true)
            imageView.setImageDrawable(circularBitmapDrawable)
        }
    })
}
fun Context.checkAccount(){
//    val accountManager= AccountManager.get(this)
//    val accounts = accountManager.getAccountsByType("ir.max_pc.seefood")
    if (Prefs.getString(TOKEN,"").isEmpty()) {
        val intent = Intent(this, LoginActivity::class.java)
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, "ir.max_pc.seefood")
        intent.putExtra(ADD_ACCOUNT, true)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}


fun Activity.hideSoftKeyboard() {
    if (currentFocus != null) {
        val inputMethodManager = getSystemService(Context
            .INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }
}

fun AppCompatActivity.getAndroidId():String{
    return try {
        Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
    } catch (e: Exception) {
        "Unknown"
    }
}


//==========================
//set CustomFont for TabLauout
//===========================
fun TabLayout.setCustomFontForTab() {
    val vg = this.getChildAt(0) as ViewGroup
    val tabsCount = vg.childCount

    for (j in 0 until tabsCount) {
        val vgTab = vg.getChildAt(j) as ViewGroup

        val tabChildsCount = vgTab.childCount

        for (i in 0 until tabChildsCount) {
            val tabViewChild = vgTab.getChildAt(i)
            if (tabViewChild is TextView) {
                //Put your font in assests folder
                //assign name of the font here (Must be case sensitive)
                tabViewChild.typeface = ResourcesCompat.getFont(this.context,R.font.iran_sans)
            }
        }
    }
}