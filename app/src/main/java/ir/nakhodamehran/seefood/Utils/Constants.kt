package ir.nakhodamehran.seefood.Utils

const val FAST_MENU = "FastMenu"
const val LANGUAGE = "language"
const val ADDRESS = "Address"
const val FIRST="first"
const val PHONE="Phone"
const val FACTOR="factor"
const val FACTORID="factorId"
const val FOOD_NAME="foodName"
const val FOOD_ID="foodID"
const val UPDATE_FACTOR="factorId"
const val ADD_ACCOUNT="AddAccount"
const val IS_COMMENT ="AddAccount"
const val DISTRICT_PRICE="DistrictPrice"
const val TOKEN="Token"

const val TOMATO_ID = 1
const val CUCUMBER_ID = 2
const val KHORMA_ID = 3
const val LEMON_ID = 4
const val PEPER_ID = 5

const val REQ_REGISTER= 11

