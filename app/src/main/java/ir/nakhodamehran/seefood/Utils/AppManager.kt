package ir.nakhodamehran.seefood.Utils

import android.app.Application
import android.content.Context
import android.content.ContextWrapper
import androidx.annotation.Keep
import androidx.databinding.ObservableField
import androidx.multidex.MultiDex
import co.ronash.pushe.Pushe
import com.pixplicity.easyprefs.library.Prefs
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.models.District
import ir.nakhodamehran.seefood.models.MenuCategory
import ir.nakhodamehran.seefood.models.Order
import ir.nakhodamehran.seefood.models.Profile
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

class AppManager:Application(){
    @Keep companion object {
        var order: Order = Order()
        var menuCategory: MenuCategory? = null
        var districts:List<District>?=null
        var authToken=ObservableField<String>("")
        var profile :Profile? = null
        //when user came back from payment to main this bool checked if was true user will go to factor fragment
        var isFromPayFragment=false
        //when user back from addAdress to ConfirmAddress system will select the latest address that user added
        var defaultAdressId:Int?=null

        var walletGiftPrice:String?=null
    }
    override fun onCreate() {
        super.onCreate()
        Pushe.initialize(this,true)
        Prefs.Builder()
            .setContext(this)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()


        CalligraphyConfig.initDefault(
            CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/IRANSans.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        )
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}
