package ir.nakhodamehran.seefood.Utils

import android.content.Intent
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import co.ronash.pushe.PusheListenerService
import org.json.JSONObject

class MyPushListener : PusheListenerService() {
    override fun onMessageReceived(customContent: JSONObject?, pushMessage: JSONObject?) {
        try {
            //if factor was 1 it means that we should udate factor list
           if(customContent?.getString("factor")=="1") {
               val intent = Intent(UPDATE_FACTOR)
               LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
           }
        } catch (e: Exception) {
            Log.d("tag", e.stackTrace.toString())

        }
    }
}