package ir.nakhodamehran.seefood.Utils.userIdentify

import android.app.Service
import android.content.Intent
import android.os.IBinder


class AccountTypeService : Service() {

  override  fun onBind(intent: Intent): IBinder {
        val authenticator = SeefoodAthenticator(this)
        return authenticator.getIBinder()
    }
}