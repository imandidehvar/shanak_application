package ir.nakhodamehran.seefood.Utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.SmsMessage


class SmsReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val data = intent.extras

        val pdus = data!!.get("pdus") as Array<Any>

        for (i in pdus.indices) {
            val smsMessage = SmsMessage.createFromPdu(pdus[i] as ByteArray)

            val sender = smsMessage.displayOriginatingAddress
            //You must check here if the sender is your provider and not another one with same text.

            val messageBody = smsMessage.messageBody

            //Pass on the text to our listener.
            if (mListener != null)
                mListener!!.messageReceived(messageBody)
        }

    }

    interface SmsListener {
        fun messageReceived(messageText: String)
    }

    companion object {
        private var mListener: SmsListener? = null

        fun bindListener(listener: SmsListener) {
            mListener = listener
        }

        fun unbindListener() {
            mListener = null
        }
    }
}