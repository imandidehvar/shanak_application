package ir.nakhodamehran.seefood.Utils

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
@GlideModule
class SvgAppModule:AppGlideModule() {
    override fun isManifestParsingEnabled(): Boolean {
        return false
    }
}