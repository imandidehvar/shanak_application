package ir.nakhodamehran.seefood.fragments.about


import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.pixplicity.easyprefs.library.Prefs
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.AppManager
import ir.nakhodamehran.seefood.Utils.LANGUAGE
import ir.nakhodamehran.seefood.Utils.retrofitFailError
import ir.nakhodamehran.seefood.Utils.toggleVisibility
import ir.nakhodamehran.seefood.models.Rule
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import kotlinx.android.synthetic.main.error_dialog.*
import kotlinx.android.synthetic.main.feedback_layout.*
import kotlinx.android.synthetic.main.fragment_sub_about.*
import kotlinx.android.synthetic.main.red_button.*
import kotlinx.android.synthetic.main.rules_layout.*
import kotlinx.android.synthetic.main.sub_about_layout.*
import okhttp3.ResponseBody
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SubAboutFragment : Fragment() {

    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sub_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val layPosition=arguments?.getInt("aboutType")

        when(layPosition){
            0->showAbout()
            1->showFeedBack()
            2->showRules()
        }
    }

    private fun showFeedBack() {
        feedBack.visibility=View.VISIBLE
        rules.visibility=View.GONE
        subAbout.visibility=View.GONE
        totalCounterText.toggleVisibility()
        confirmText.text=getString(R.string.confirm)
        confirm_btn.setOnClickListener {

            if (feedBackInput.text!!.isNotBlank()) {
                confirm_progress.toggleVisibility()
                confirmText.toggleVisibility()
                seafoodAPIService.sendFeedBack(AppManager.authToken.get()!!,mapOf("message" to feedBackInput.text.toString()))
                    .enqueue(object : Callback<ResponseBody>{
                        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                            it.retrofitFailError(t){it.performClick()}
                            confirm_progress.toggleVisibility()
                            confirmText.toggleVisibility()
                        }

                        override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                            if(response.isSuccessful) {
                                feedBackInput.setText("")
                                val builder = AlertDialog.Builder(activity!!)
                                builder.setView(R.layout.error_dialog)
                                val dialog = builder.create()
                                dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                                dialog.show()
                                dialog.title.text= resources.getString(R.string.message)
                                dialog.message.text = resources.getString(R.string.sent)
                                dialog.negativeButton.toggleVisibility()
                                dialog.positiveButton.text = getString(R.string.okey)
                                dialog.positiveButton.setOnClickListener {
                                    dialog.dismiss()
                                }
                            }

                            confirm_progress.toggleVisibility()
                            confirmText.toggleVisibility()
                        }
                    })
            } else
                activity?.toast(getString(R.string.fillInput))
        }

    }

    private fun showAbout() {
        feedBack.visibility=View.GONE
        rules.visibility=View.GONE
        subAbout.visibility=View.VISIBLE
        aboutText.setMovementMethod(ScrollingMovementMethod())

        progress.toggleVisibility()
        seafoodAPIService.getAboutMessage(mapOf("lang" to Prefs.getString(LANGUAGE,"fa"))).enqueue(object :Callback<Map<String,String>>{
            override fun onFailure(call: Call<Map<String, String>>, t: Throwable) {
                try {
                    progress.toggleVisibility()
                } catch (e: Exception) {
                }
            }

            override fun onResponse(call: Call<Map<String, String>>, response: Response<Map<String, String>>) {
                try {
                    progress.toggleVisibility()
                    if(response.isSuccessful)
                        aboutText.text= response.body()?.get("aboutUs") ?: ""
                } catch (e: Exception) {
                }
            }
        })

        call_btn.setOnClickListener {
            val phone = "025-31880"
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
            startActivity(intent)
        }
    }

    private fun showRules() {
        feedBack.visibility=View.GONE
        subAbout.visibility=View.GONE
        rules.visibility=View.VISIBLE
        rule_rv.adapter=RuleAdapter(Rule.ruleList)
    }
}
