package ir.nakhodamehran.seefood.fragments.address


import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.ADDRESS
import ir.nakhodamehran.seefood.Utils.AppManager
import ir.nakhodamehran.seefood.Utils.StatusBarUtils
import ir.nakhodamehran.seefood.Utils.toggleVisibility
import ir.nakhodamehran.seefood.activities.main.MainActivity
import ir.nakhodamehran.seefood.models.Address
import ir.nakhodamehran.seefood.models.District
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.fragment_add_adress.*
import org.jetbrains.anko.toast


class AddAdressFragment : Fragment() {

    private lateinit var viewModel: AddressViewModel
    private var isEdite = false
    private var isConfirmCliced = false
    private var address: Address? = null
    private var selectedDistrict: District? = null
    val districtTitle = mutableListOf<String>()



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AddressViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_adress, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StatusBarUtils.setPaddingSmart(activity, topSpac)

        activity?.let {
            (it as MainActivity).binding.content.pageTitle.text=getString(R.string.myAddress)
        }
        districtTitle.add(getString(R.string.selectRegion))
        setupInputDrawables()
        configSpinner()
        setupEdit()
        setupClickListeners()
    }

    private fun setupInputDrawables() {
        var drawable = resources.getDrawable(R.drawable.ic_call)
        drawable = DrawableCompat.wrap(drawable)
        DrawableCompat.setTint(drawable, resources.getColor(android.R.color.holo_orange_light))
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN)
        phoneInput.editText?.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)

        drawable = resources.getDrawable(R.drawable.ic_place)
        DrawableCompat.setTint(drawable, resources.getColor(android.R.color.holo_orange_light))
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN)
        addressInput.editText?.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)
    }

    private fun setupEdit() {
        address = arguments?.getParcelable(ADDRESS)
        if (address != null) {
            isEdite = true
            titleInput.editText?.setText(address?.title)
            addressInput.editText?.setText(address?.address)
            phoneInput.editText?.setText(address?.phone)
            AppManager.districts?.forEach {
                if(it.Id==address?.regionId)
                    for(i in 0 until districtTitle.size)
                        if(districtTitle[i]==it.title)
                            districtSpinner.setSelection(i)



            }
        }
    }

    private fun setupClickListeners() {
        confirm_btn.setOnClickListener {
            if (!isConfirmCliced) {

                if (selectedDistrict == null) {
                    activity?.toast(getString(R.string.district_err))
                    return@setOnClickListener
                }
                when {
                    titleInput.editText!!.text.isEmpty()->{
                        titleInput.error=getString(R.string.add_titleErr)
                        return@setOnClickListener
                    }
                    addressInput.editText!!.text.isEmpty() -> {
                        addressInput.error = getString(R.string.addAddressErr)
                        return@setOnClickListener
                    }
                    phoneInput.editText!!.text.length in 1..7 -> {
                        phoneInput.error =getString(R.string.formatPhoneErr)
                        return@setOnClickListener
                    }
                    else -> {
                        confirm_txt.toggleVisibility()
                        progress.toggleVisibility()

                        if (isEdite)
                            viewModel.editAddress(confirm_btn, setupAddress(), {
                                activity?.toast(getString(R.string.updatedAddress))
                                activity?.onBackPressed()
                            }) {
                                confirm_btn.performClick()
                            }
                        else {
                            viewModel.addAddress(confirm_btn, setupAddress(), {
                                activity?.toast(getString(R.string.succesfullAddAddress))
                                activity?.onBackPressed()
                               AppManager.defaultAdressId=it
                            }) {
                                confirm_btn.performClick()
                            }
                        }
                        isConfirmCliced = true
                    }
                }

            }
        }

        cancel_btn.setOnClickListener { activity?.onBackPressed() }
    }


    private fun configSpinner() {
     AppManager.districts?.forEach { districtTitle.add(it.title) }

        val adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_dropdown_item, districtTitle)
        districtSpinner.adapter = adapter

        districtSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                selectedDistrict = AppManager.districts?.find { it?.title == districtTitle[position] }
            }
        }
    }

    private fun setupAddress(): Address {
        val addressFill = Address()
        addressFill.Id = address?.Id ?: -1
        addressFill.phone = phoneInput?.editText?.text.toString()
        addressFill.title = titleInput?.editText?.text.toString()
        addressFill.address = addressInput?.editText?.text.toString()
        addressFill.regionId = selectedDistrict?.Id ?: -1
        return addressFill
    }
}
