package ir.nakhodamehran.seefood.fragments.factors

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.StatusBarUtils
import ir.nakhodamehran.seefood.Utils.UPDATE_FACTOR
import ir.nakhodamehran.seefood.activities.main.MainActivity
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.factors_fragment.*


class FactorsFragment : Fragment() {

    private lateinit var viewModel: FactorsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this).get(FactorsViewModel::class.java)
        return inflater.inflate(R.layout.factors_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        activity?.let {
            (it as MainActivity).binding.content.pageTitle.text=getString(R.string.myFactor)
            it.binding.content.backButton.visibility=View.VISIBLE
        }
        StatusBarUtils.setPaddingSmart(activity, topSpac)
        configListOrder()
        refreshLayout.setColorSchemeColors(Color.YELLOW)
        refreshLayout.setOnRefreshListener { configListOrder() }

        LocalBroadcastManager.getInstance(activity!!).registerReceiver(
            updateReciver,
            IntentFilter(UPDATE_FACTOR)
        )

    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(activity!!).unregisterReceiver(updateReciver)
    }

    private fun configListOrder() {
        emptyHolder.visibility=View.GONE
        refreshLayout.isRefreshing = true
        viewModel.getFactors(factor_rv,{
            refreshLayout.isRefreshing=false
            val adapter=FactorAdapter(it)
            factor_rv.adapter=adapter
            if(it.isEmpty())
                emptyHolder.visibility=View.VISIBLE
        },::configListOrder)

    }


    val updateReciver=object :BroadcastReceiver(){
        override fun onReceive(mMessageReceiver: Context?, intent: Intent?) {
            try {
                configListOrder()
            } catch (e: Exception) {
            }
        }
    }

}
