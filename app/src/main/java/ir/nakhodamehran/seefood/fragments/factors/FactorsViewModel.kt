package ir.nakhodamehran.seefood.fragments.factors

import android.view.View
import androidx.lifecycle.ViewModel
import com.pixplicity.easyprefs.library.Prefs
import ir.nakhodamehran.seefood.Utils.AppManager
import ir.nakhodamehran.seefood.Utils.LANGUAGE
import ir.nakhodamehran.seefood.Utils.retrofitFailError
import ir.nakhodamehran.seefood.models.Factor
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FactorsViewModel : ViewModel() {
    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }


    fun getFactors(view: View, success:(List<Factor>)->Unit, retry:()->Unit){
        seafoodAPIService.getFactor(AppManager.authToken.get()!!).enqueue(object :Callback<List<Factor>>{
            override fun onFailure(call: Call<List<Factor>>, t: Throwable) {
                view.retrofitFailError(t,retry)
            }

            override fun onResponse(call: Call<List<Factor>>, response: Response<List<Factor>>) {
                if(response.isSuccessful)
                    try {
                        response.body()?.let {
                            it.forEach {
                                //set arabic for pay status
                                if(Prefs.getString(LANGUAGE,"fa")=="ar") {
                                    when {
                                        it.payway=="نقدی" -> it.payway = "نقداً"
                                        it.payway=="درگاه بانکی" -> it.payway = "online"
                                        else -> it.payway = "محفظتي"
                                    }
                                }
                            }
                            success(it) }
                    }catch (e :Exception){

                    }
            }
        })
    }
}
