package ir.nakhodamehran.seefood.fragments.confirms.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.databinding.ConfirmItemBinding
import ir.nakhodamehran.seefood.fragments.confirms.viewModels.ConfirmViewModel
import ir.nakhodamehran.seefood.models.DishOrder
import ir.nakhodamehran.seefood.models.ListMenu
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import kotlinx.android.synthetic.main.confirm_item.view.*

class ConfirmAdapter(
    val mainList: MutableList<DishOrder>,
    val dessertList: MutableList<ListMenu>,
    val viewModel: ConfirmViewModel,
    val showEmptyState: () -> Unit
) : RecyclerView.Adapter<ConfirmAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ConfirmItemBinding.inflate(inflater, parent, false)
        return MyViewHolder(binding.root)
    }


    override fun getItemCount(): Int = mainList.size + dessertList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        clearImages(holder)
        val binding = DataBindingUtil.getBinding<ConfirmItemBinding>(holder.itemView)
        val viewModel= ConfirmViewModel()
        binding?.viewModel = viewModel
        binding?.executePendingBindings()
        try {
            val mainItem = mainList[position]
            viewModel.getMainFoodTitle(mainItem.mainID)
            binding?.main = mainItem

            if(mainItem.side.size==5)
                AppManager.menuCategory?.mainList?.forEach {
                    if (it.id == mainItem.mainID)
                        Glide.with(holder.itemView.context)
                            .load(RetrofitClient.BASE_URL + it.fastImage)
                            .into(holder.itemView.dish_img)
                    // was fish for custom
                }

            else{
                AppManager.menuCategory?.mainList?.forEach {
                    if (it.id == mainItem.mainID)
                        Glide.with(holder.itemView.context)
                            .load(RetrofitClient.BASE_URL + it.image)
                            .into(holder.itemView.fish)
                }

                mainItem.side.forEach { selectedSideId ->
                    AppManager.menuCategory?.sideList?.forEach { side ->
                    if (selectedSideId == side.id)
                        when (side.id) {
                            KHORMA_ID -> {
                                Glide.with(holder.itemView.context)
                                    .load(RetrofitClient.BASE_URL + side.image)
                                    .into(holder.itemView.khormaPlace)
                            }
                            LEMON_ID -> {
                                Glide.with(holder.itemView.context)
                                    .load(RetrofitClient.BASE_URL + side.image)
                                    .into(holder.itemView.lemonPlace)
                            }
                            CUCUMBER_ID -> {
                                Glide.with(holder.itemView.context)
                                    .load(RetrofitClient.BASE_URL + side.image)
                                    .into(holder.itemView.cucumberPlace)
                            }
                            TOMATO_ID -> {
                                Glide.with(holder.itemView.context)
                                    .load(RetrofitClient.BASE_URL + side.image)
                                    .into(holder.itemView.tomatoPlace)
                            }
                            PEPER_ID -> {
                                Glide.with(holder.itemView.context)
                                    .load(RetrofitClient.BASE_URL + side.image)
                                    .into(holder.itemView.potatoPlace)
                            }
                        }
                }

            }
            }

//
//
        } catch (e: Exception) {
            val dessert = dessertList[position - mainList.size]
            binding?.dessert = dessert
            holder.itemView.dish_img.setImageResource(android.R.color.transparent)
            Glide.with(holder.itemView.context)
                .load(RetrofitClient.BASE_URL + dessert.image)
                .into(holder.itemView.dessertPlace)
        }
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var binding: ConfirmItemBinding? = null

        init {
            binding = DataBindingUtil.getBinding(itemView)
            binding?.plusFood?.setOnClickListener(this)
            binding?.minusFood?.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            val pos = adapterPosition
            when (view?.id) {
                R.id.plusFood -> {
                    try {
                        val mainItem = mainList[pos]
                        ir.nakhodamehran.seefood.Utils.DataBindingAdapter.setCounter(binding?.counterFood, (++mainItem.count).toString())
                        viewModel.totalPrice = viewModel.totalPrice?.plus(mainItem.price.toLong())
                    } catch (e: Exception) {
                        val dessertItem = dessertList[pos - mainList.size]
                        ir.nakhodamehran.seefood.Utils.DataBindingAdapter.setCounter(binding?.counterFood, (++dessertItem.count).toString())
                        viewModel.totalPrice = viewModel.totalPrice?.plus(dessertItem.price!!.toLong())
                    }
                }
                R.id.minusFood -> {
                    try {
                        val mainItem = mainList[pos]
                        if (mainItem.count != 1) {
                            ir.nakhodamehran.seefood.Utils.DataBindingAdapter.setCounter(binding?.counterFood, (--mainItem.count).toString())

                            ir.nakhodamehran.seefood.Utils.AppManager.order.dishes.map {
                                if (it.mainID == mainItem.mainID)
                                    it.count = mainItem.count
                            }

                        }else {
                            ir.nakhodamehran.seefood.Utils.AppManager.order.dishes.remove(mainItem)
                            mainList.remove(mainItem)
                            notifyItemRemoved(pos)
                            notifyItemRangeChanged(pos, mainList.size + dessertList.size)
                        }
                        viewModel.totalPrice = viewModel.totalPrice?.minus(mainItem.price.toLong())
                    } catch (e: Exception) {
                        val dessertItem = dessertList[pos - mainList.size]
                        if (dessertItem.count != 1) {
                            ir.nakhodamehran.seefood.Utils.DataBindingAdapter.setCounter(binding?.counterFood, (--dessertItem.count).toString())

                            if(dessertItem.isDessert!!)
                            AppManager.order.desserts.map {
                                if (it.id == dessertItem.id)
                                    it.count = dessertItem.count
                            }
                            else
                                AppManager.order.drinks.map {
                                    if (it.id == dessertItem.id)
                                        it.count = dessertItem.count
                                }

                        } else {
                            if(dessertItem.isDessert!!)
                            AppManager.order.desserts.remove(dessertItem)
                            else
                             AppManager.order.drinks.remove(dessertItem)

                            dessertList.remove(dessertItem)
                            notifyItemRemoved(pos)
                            notifyItemRangeChanged(pos, mainList.size + dessertList.size)
                        }
                        viewModel.totalPrice = viewModel.totalPrice?.minus(dessertItem.price!!.toLong())
                    }

                    if(mainList.isEmpty() && dessertList.isEmpty())
                        showEmptyState()
                }
            }
        }

    }

    private fun clearImages(holder: MyViewHolder) {
        holder.itemView.tomatoPlace.setImageResource(android.R.color.transparent)
        holder.itemView.lemonPlace.setImageResource(android.R.color.transparent)
        holder.itemView.cucumberPlace.setImageResource(android.R.color.transparent)
        holder.itemView.potatoPlace.setImageResource(android.R.color.transparent)
        holder.itemView.khormaPlace.setImageResource(android.R.color.transparent)
        holder.itemView.fish.setImageResource(android.R.color.transparent)
        holder.itemView.dessertPlace.setImageResource(android.R.color.transparent)
        holder.itemView.dish_img.setImageResource(R.drawable.plate)
    }
}