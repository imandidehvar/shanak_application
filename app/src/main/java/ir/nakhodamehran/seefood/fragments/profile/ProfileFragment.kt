package ir.nakhodamehran.seefood.fragments.profile


import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import co.ronash.pushe.Pushe
import com.pixplicity.easyprefs.library.Prefs
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.activities.main.MainActivity
import ir.nakhodamehran.seefood.databinding.FragmentProfileBinding
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.select_language_dialog.*
import okhttp3.ResponseBody
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ProfileFragment : Fragment() {

    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }
    private lateinit var binding:FragmentProfileBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.let {
            (it as MainActivity).binding.content.pageTitle.text=getString(R.string.myProfile)
            it.binding.content.backButton.visibility=View.VISIBLE
        }
        binding= FragmentProfileBinding.inflate(inflater,container,false)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
     StatusBarUtils.setPaddingSmart(activity, topSpac)
        binding.profile=AppManager.profile!!
        activity?.displayCercularImage(RetrofitClient.BASE_URL+AppManager.profile!!.avatarImage,binding.logo)

        confirm_btn.setOnClickListener { updateProfile() }
        languageButton.setOnClickListener {
            val builder = AlertDialog.Builder(activity!!)
            builder.setView(R.layout.select_language_dialog)
            val dialog = builder.create()
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()

            if(Prefs.getString(LANGUAGE,"fa")=="fa")
                dialog.perButton.isChecked=true
            else
                dialog.arabButton.isChecked=true

          dialog.positiveButton.setOnClickListener {
              if(dialog.languageGroups.checkedRadioButtonId==R.id.perButton){
                  Prefs.putString(LANGUAGE,"fa")
                  Handler().postDelayed({activity?.restartApp() },300)
              } else if(dialog.languageGroups.checkedRadioButtonId==R.id.arabButton){
                  Prefs.putString(LANGUAGE,"ar")
                  Handler().postDelayed({activity?.restartApp() },300)
              }
              dialog.dismiss()
          }
        }

        ownerAddressFragment.setOnClickListener {
            it.findNavController().navigate(R.id.action_to_ownerAddressFragment)
        }

        changeAvatar.setOnClickListener {
            it.findNavController().navigate(R.id.avatarFragment)
        }
    }


    private fun updateProfile(){
        try {
            confirm_progress.toggleVisibility()
            confirmText.toggleVisibility()
            AppManager.profile?.deviceId=Pushe.getPusheId(activity)
            AppManager.profile?.name=profileName.editText?.text.toString()
            seafoodAPIService.updateUser(AppManager.authToken.get()!!,AppManager.profile!!).enqueue(object: Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        confirm_btn.retrofitFailError(t){
                            updateProfile()
                        }
                    confirm_progress.toggleVisibility()
                    confirmText.toggleVisibility()
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    try {
                        if(response.isSuccessful){
                            activity?.toast(getString(R.string.updateProfileSuccess))
                            (activity as MainActivity).getProfile()
                        }

                        confirm_progress.toggleVisibility()
                        confirmText.toggleVisibility()
                    } catch (e: Exception) {
                    }

                }

            })
        } catch (e: Exception) {
        }
    }

}
