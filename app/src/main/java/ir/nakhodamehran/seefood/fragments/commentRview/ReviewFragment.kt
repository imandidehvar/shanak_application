package ir.nakhodamehran.seefood.fragments.commentRview


import android.graphics.Bitmap
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.SurfaceHolder
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.fragment.app.Fragment
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.github.florent37.viewanimator.ViewAnimator
import com.pixplicity.easyprefs.library.Prefs
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.models.Review
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import kotlinx.android.synthetic.main.fragment_review.*
import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ReviewFragment : Fragment(), SurfaceHolder.Callback, MediaPlayer.OnPreparedListener {


    private var mp: MediaPlayer? = null
    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_review, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        configReview()

        refreshLayout.setColorSchemeColors(
            ContextCompat.getColor(context!!, android.R.color.holo_orange_light),
            ContextCompat.getColor(context!!, R.color.colorAccent)
        )

        refreshLayout.setOnRefreshListener {
            configReview()
        }
    }




    private fun configReview() {
        refreshLayout.isRefreshing=true
        comment_box.visibility=View.GONE
        reviewNot_txt.visibility=View.GONE
        val foodId = arguments?.getInt(FOOD_ID)
        seafoodAPIService.getReview(
            AppManager.authToken.get()!!,
            mapOf("lang" to Prefs.getString(LANGUAGE, "fa"), "product_id" to foodId.toString())
        ).enqueue(object : Callback<Review> {
            override fun onFailure(call: Call<Review>, t: Throwable) {
                try {
                    cardView.retrofitFailError(t){configReview()}
                    refreshLayout.isRefreshing=false
                } catch (e: Exception) {
                }
            }

            override fun onResponse(call: Call<Review>, response: Response<Review>) {
                if (response.isSuccessful)
                    prepareContent(response.body()!!)


                refreshLayout.isRefreshing=false

            }

        })
    }


    private fun prepareContent(data: Review) {
        try {

            if(!data.text1.isNullOrBlank())
                comment_box.visibility=View.VISIBLE

            else
                reviewNot_txt.visibility=View.VISIBLE

            content_one.setHtml(data.text1, HtmlHttpImageGetter(content_one))
            if(!data.text2.isNullOrBlank())
            content_two.setHtml(data.text2, HtmlHttpImageGetter(content_two))
            if(!data.text3.isNullOrEmpty())
            content_three.setHtml(data.text3, HtmlHttpImageGetter(content_three))

            //set title
            title.text = data.title
            title.typeface=ResourcesCompat.getFont(context!!,R.font.iran_sans_bold)

            if(data.video.isNullOrBlank())
                videoLay.visibility=View.GONE
            else
            prepareVideo(RetrofitClient.BASE_URL + data.video)

            GlideApp.with(this).asBitmap().load(RetrofitClient.BASE_URL + data.img1)
                .into(object : BitmapImageViewTarget(image_one) {
                    override fun setResource(resource: Bitmap?) {
                        try {
                            val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(resources, resource)
                            circularBitmapDrawable.cornerRadius = 200f
                            image_one.setImageDrawable(circularBitmapDrawable)
                        } catch (e: Exception) {
                        }
                    }
                })

            GlideApp.with(this).asBitmap().load(RetrofitClient.BASE_URL + data.img2)
                .into(object : BitmapImageViewTarget(image_two) {
                    override fun setResource(resource: Bitmap?) {
                        try {
                            val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(resources, resource)
                            circularBitmapDrawable.cornerRadius = 200f
                            image_two.setImageDrawable(circularBitmapDrawable)
                        } catch (e: Exception) {
                        }
                    }
                })
        } catch (e: Exception) {
        }
    }


    private fun prepareVideo(vidoUrl: String) {

        videoLay.afterMeasured {
           val width=this.measuredWidth
            val height=(width*9)/16
            val layParam=this.layoutParams

            layParam.height=height
            this.layoutParams=layParam
            this.requestLayout()
        }

        videoLay.setOnClickListener {
             ViewAnimator.animate(controlVideo_btn).fadeIn().duration(100).start()
            mp?.let {
                    Handler().postDelayed({
                        if(it.isPlaying)
                        ViewAnimator.animate(controlVideo_btn).fadeOut().duration(500).start()
                    }, 1500)
                }

        }

        val vidHolder = videoView.holder

        controlVideo_btn.setOnClickListener {
            if (mp == null) {
                controlVideo_btn.toggleVisibility()
                progress.toggleVisibility()

                mp = MediaPlayer()
                mp?.setDisplay(vidHolder)
                object :Thread(){
                    override fun run() {
                        mp?.setDataSource(vidoUrl)
                        mp?.prepare()
                    }

                }.start()
                mp?.setOnPreparedListener(this)
                mp?.setAudioStreamType(AudioManager.STREAM_MUSIC)
                return@setOnClickListener
            }

            if (mp!!.isPlaying) {
                controlVideo_btn.setImageResource(R.drawable.ic_play_arrow_24dp)
                ViewAnimator.animate(controlVideo_btn).fadeIn().duration(100).start()
                mp?.pause()
            } else {
                controlVideo_btn.setImageResource(R.drawable.ic_pause_24dp)
                Handler().postDelayed({
                    ViewAnimator.animate(controlVideo_btn).fadeOut().duration(500).start()
                }, 1500)
                mp?.start()
            }

        }

    }



    override fun surfaceChanged(p0: SurfaceHolder?, p1: Int, p2: Int, p3: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun surfaceDestroyed(p0: SurfaceHolder?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun surfaceCreated(p0: SurfaceHolder?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPrepared(mp: MediaPlayer?) {
        mp?.start()

        controlVideo_btn.toggleVisibility()
        progress.toggleVisibility()

        controlVideo_btn.setImageResource(R.drawable.ic_pause_24dp)
        Handler().postDelayed({
            ViewAnimator.animate(controlVideo_btn).fadeOut().duration(500).start()
        }, 1500)
    }

    override fun onDestroy() {
        super.onDestroy()
        mp?.let {
            if (it.isPlaying) {
                it.stop()
                it.release()
                mp = null
            }
        }

    }
}
