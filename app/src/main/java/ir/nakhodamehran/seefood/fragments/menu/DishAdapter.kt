package ir.nakhodamehran.seefood.fragments.menu

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.models.Dish
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import kotlinx.android.synthetic.main.dish_item.view.*

class DishAdapter(var items: List<Dish>) : RecyclerView.Adapter<DishAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.dish_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.itemView.tomatoPlace.setImageResource(android.R.color.transparent)
        holder.itemView.lemonPlace.setImageResource(android.R.color.transparent)
        holder.itemView.cucumberPlace.setImageResource(android.R.color.transparent)
        holder.itemView.potatoPlace.setImageResource(android.R.color.transparent)
        holder.itemView.khormaPlace.setImageResource(android.R.color.transparent)



        val dish = items[position]

        if (dish.main != 0) {
            val selectedFood = AppManager.menuCategory?.mainList?.find { dish.main == it.id }
                Glide.with(holder.itemView.context)
                    .load(RetrofitClient.BASE_URL + selectedFood?.image)
                    .into(holder.itemView.fish)

        } else
            holder.itemView.fish.setImageResource(android.R.color.transparent)



        dish.side.forEach {
            when (it.id) {
                KHORMA_ID -> {
                    Glide.with(holder.itemView.context)
                        .load(RetrofitClient.BASE_URL + it.image)
                        .into(holder.itemView.khormaPlace)
                }
                LEMON_ID -> {
                    Glide.with(holder.itemView.context)
                        .load(RetrofitClient.BASE_URL + it.image)
                        .into(holder.itemView.lemonPlace)
                }
                CUCUMBER_ID -> {
                    Glide.with(holder.itemView.context)
                        .load(RetrofitClient.BASE_URL + it.image)
                        .into(holder.itemView.cucumberPlace)
                }
                TOMATO_ID -> {
                    Glide.with(holder.itemView.context)
                        .load(RetrofitClient.BASE_URL + it.image)
                        .into(holder.itemView.tomatoPlace)
                }
                PEPER_ID -> {
                    Glide.with(holder.itemView.context)
                        .load(RetrofitClient.BASE_URL + it.image)
                        .into(holder.itemView.potatoPlace)
                }
            }
        }
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}