package ir.nakhodamehran.seefood.fragments.factorDetail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.databinding.FactorDetailItemBinding
import ir.nakhodamehran.seefood.models.ListMenu
import kotlinx.android.synthetic.main.factor_detail_item.view.*

class FactorDetailAdapter(val menuList:MutableList<ListMenu>) : RecyclerView.Adapter<FactorDetailAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =FactorDetailItemBinding.inflate(inflater, parent, false)
        return MyViewHolder(binding.root)
    }

    override fun getItemCount(): Int= menuList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val binding= DataBindingUtil.getBinding<FactorDetailItemBinding>(holder.itemView)
        val item=menuList[position]
        binding?.data=item
        holder.itemView.title.typeface=ResourcesCompat.getFont(holder.itemView.context,R.font.iran_sans_bold)
        holder.itemView.mainCount.text=holder.itemView.context.getString(R.string.count) +" "+ item.count.toString()
        holder.itemView.price.text=holder.itemView.context.getString(R.string.unitPrice)+" "+ item.price.toString()+" تومان"
    }


    class MyViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView)
}