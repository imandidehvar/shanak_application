package ir.nakhodamehran.seefood.fragments.factorDetail

import android.content.Context
import com.google.gson.Gson
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.AppManager
import ir.nakhodamehran.seefood.models.Factor
import ir.nakhodamehran.seefood.models.ListMenu
import ir.nakhodamehran.seefood.models.Order


class FactorDetailViewModel(val context: Context,private val factor:Factor){

    fun getMenulist() : MutableList<ListMenu>{
        val menuList= mutableListOf<ListMenu>()
        val selectedOrder = Gson().fromJson<Order>(factor.orderItems, Order::class.java)

        //getMainFood
        for (orderedMain in selectedOrder.dishes)
            for (mainFood in AppManager.menuCategory!!.mainList!!)
                if (mainFood.id == orderedMain.mainID) {

                    val description=StringBuilder(context.resources.getString(R.string.side)).append(" ")
                    //find food sideNames
                    orderedMain.side.forEach {foodSideId->
                        AppManager.menuCategory!!.sideList?.forEach {side->
                            if(foodSideId==side.id)
                                description.append(side.title).append(",")
                        }
                    }
                    description.deleteCharAt(description.length-1)
                    //create foodObject
                    val food=ListMenu(orderedMain.mainID,mainFood.title,
                        description.toString(),
                        price = orderedMain.price,
                        offProduct = mainFood.offProduct,
                        image = mainFood.fastImage,
                        count = orderedMain.count)

                    menuList.add(food)
                    break
                }


        //getDessert
        for (orderedDessert in selectedOrder.desserts)
            for (dessert in AppManager.menuCategory!!.dessertList!!)
                if (dessert.id  == orderedDessert.id) {
                    val food=ListMenu(orderedDessert.id,dessert.title,
                        dessert.description,
                        price = dessert.price,
                        offProduct = dessert.offProduct,
                        image =  dessert.image,
                        isDessert = true,
                        count = orderedDessert.count)

                    menuList.add(food)
                    break
                }



        //getDrinks
        for (orderedDrinks in selectedOrder.drinks)
            for (drink in AppManager.menuCategory!!.drinkList!!)
                if (drink.id == orderedDrinks.id) {

                    val food=ListMenu(orderedDrinks.id,drink.title,
                        drink.description,
                        price = drink.price,
                        offProduct = drink.offProduct,
                        image = drink.image,
                        count = orderedDrinks.count)

                    menuList.add(food)
                    break
                }

        return menuList
    }
}