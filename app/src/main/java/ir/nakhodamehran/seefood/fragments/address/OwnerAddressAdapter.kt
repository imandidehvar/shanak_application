package ir.nakhodamehran.seefood.fragments.address

import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.ADDRESS
import ir.nakhodamehran.seefood.models.Address
import kotlinx.android.synthetic.main.owner_address_item.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.bundleOf

class OwnerAddressAdapter(val viewModel:AddressViewModel,val addressList:MutableList<Address>,val showEmptyState:()->Unit):RecyclerView.Adapter<OwnerAddressAdapter.MyViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.owner_address_item,parent,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int=addressList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val address=addressList[position]
        val drawable = holder.itemView.resources.getDrawable(R.drawable.ic_place)
        DrawableCompat.setTint(drawable, ContextCompat.getColor(holder.itemView.context,android.R.color.holo_orange_light))
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN)
        holder.itemView.addressTitle?.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)
        holder.itemView.addressTitle.text=address.address
    }


    inner class MyViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        init {
            itemView.editButton.setOnClickListener {
                val address=addressList[adapterPosition]
                it.findNavController().navigate(R.id.action_owner_to_addAdressFragment, bundleOf(ADDRESS to address))
            }

            itemView.deleteButton.setOnClickListener {view->
                view.context.alert {
                    title=view.context.getString(R.string.verify_remove_address)
                    positiveButton(view.context.getString(R.string.yes)){
                        val position=adapterPosition
                        viewModel.deleteAddress(view, addressList[position].Id!!,{
                            addressList.removeAt(position)
                            notifyItemRemoved(position)
                            notifyItemRangeChanged(position,addressList.size)
                            if(addressList.isEmpty())
                                showEmptyState()
                        }){view.performClick()}
                    }

                    negativeButton(view.context.getString(R.string.noap)){
                        it.dismiss()
                    }
                }.show()

            }
        }
    }
}
