package ir.nakhodamehran.seefood.fragments.listMenu

import android.graphics.Color
import android.os.Handler
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.StrikethroughSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.github.florent37.viewanimator.ViewAnimator
import ir.nakhodamehran.seefood.BR
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.databinding.MenuListItemBinding
import ir.nakhodamehran.seefood.databinding.TitleItemBinding
import ir.nakhodamehran.seefood.models.DishOrder
import ir.nakhodamehran.seefood.models.ListMenu
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import kotlinx.android.synthetic.main.menu_list_item.view.*
import kotlinx.android.synthetic.main.title_item.view.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.bundleOf

class ListMenuAadapter(val menuList: List<ListMenu>, val funCounter: (Boolean) -> Unit) :
    RecyclerView.Adapter<ListMenuAadapter.MyViewHolder>(),AnkoLogger {

    private var handler:Handler = Handler()

    override fun getItemViewType(position: Int): Int {
        return if (menuList[position].isCategoryName!!)
            1
        else
            0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = if (viewType == 0)
            MenuListItemBinding.inflate(inflater, parent, false)
        else
            TitleItemBinding.inflate(inflater, parent, false)

        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int = menuList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val menuItem = menuList[position]
        val binding = holder.getListBinding()
        updateList(position)
        binding.setVariable(BR.menuList, menuItem)

        if(menuItem.sideList.isNotEmpty()){

            if(menuItem.time!=0) {
                holder.itemView.specialHolder.visibility = View.VISIBLE
                handler.removeCallbacks(menuItem.timerRunnable as TimerRunnable?)
                menuItem.timerRunnable= TimerRunnable(handler,holder,menuItem){
                    notifyItemChanged(position)
                }
                handler.postDelayed(menuItem.timerRunnable as TimerRunnable,100)
            }
            else
                holder.itemView.specialHolder.visibility = View.GONE

            holder.itemView.reviewComment_box.visibility=View.VISIBLE
            clearImages(holder)
            holder.itemView.dish_img.visibility=View.VISIBLE
            AppManager.menuCategory?.mainList?.forEach {
                if (it.id == menuItem.id)
                    Glide.with(holder.itemView.context)
                        .load(RetrofitClient.BASE_URL + it.fastImage)
                        .into(holder.itemView.dish_img)
            }
        }

        else if(!menuItem.isCategoryName!!){
            holder.itemView.specialHolder.visibility=View.GONE
            holder.itemView.reviewComment_box.visibility=View.GONE
            clearImages(holder)
            holder.itemView.dish_img.visibility=View.GONE
            Glide.with(holder.itemView.context)
                .load(RetrofitClient.BASE_URL + menuItem.image)
                .into(holder.itemView.dessertPlace)
        }
        try {
//
            var builder=SpannableString(String.format("%,d", menuItem.price?.toLong()) + " تومان")

            if(menuItem.offProduct!="0"){
                holder.itemView.offPrice.visibility=View.VISIBLE
                holder.itemView.offPrice.text=String.format("%,d",menuItem.offProduct.toLong()) + " تومان"

                builder.setSpan(
                    StrikethroughSpan(), 0,
                    builder.toString().length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )

                builder.setSpan(
                    ForegroundColorSpan(Color.RED),0,
                    builder.toString().length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )

                holder.itemView.price.setText(builder, TextView.BufferType.SPANNABLE)
            }
            else {
                holder.itemView.offPrice.visibility = View.GONE
                holder.itemView.price.text = String.format("%,d", menuItem.price?.toLong()) + " تومان"

            }
        } catch (e: Exception) {
        }

    }

    //this is when user came back from confirmOrderFragment
    //region UpdatList
    private fun updateList(position: Int) {
        //MainFood
        if(menuList[position].sideList.isNotEmpty())
            AppManager.order.dishes.forEach {
               if(it.mainID == menuList[position].id){
                   menuList[position].isCountered = true
                   menuList[position].count = it.count
                   return@forEach
               }
            }


        //Dessert
        if (AppManager.order.desserts.isNotEmpty()) {
            AppManager.order.desserts.forEach {
                if (it.id == menuList[position].id) {
                    menuList[position].isCountered = true
                    menuList[position].count = it.count
                    return@forEach
                }
            }
        }
        //drinks
        if (AppManager.order.drinks.isNotEmpty()) {
            AppManager.order.drinks.forEach {
                if (it.id == menuList[position].id) {
                    menuList[position].isCountered = true
                    menuList[position].count = it.count
                    return@forEach
                }
            }
        }
    }
    //endregion





    inner class MyViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        var selectedPosition: Int = -1
        var menuBinding: MenuListItemBinding? = null
        init {

            if (binding is MenuListItemBinding) {
                menuBinding = binding
                menuBinding?.title?.typeface = ResourcesCompat.getFont(itemView.context, R.font.iran_sans_bold)
                menuBinding?.secondText?.typeface = ResourcesCompat.getFont(itemView.context, R.font.iran_sans_bold)
                menuBinding?.minuteText?.typeface = ResourcesCompat.getFont(itemView.context, R.font.iran_sans_bold)
                menuBinding?.hourText?.typeface = ResourcesCompat.getFont(itemView.context, R.font.iran_sans_bold)
                menuBinding?.specialText?.typeface = ResourcesCompat.getFont(itemView.context, R.font.iran_sans_bold)
            } else {
                binding.root.titleCategory.typeface = ResourcesCompat.getFont(itemView.context, R.font.iran_sans_bold)
            }

            menuBinding?.reviewTxt?.setOnClickListener {
                val menuItem = menuList[adapterPosition]

                it.findNavController().navigate(R.id.action_to_commentRviewFragment, bundleOf(FOOD_ID to menuItem.id,
                    FOOD_NAME to menuItem.title, IS_COMMENT to false))
            }

            menuBinding?.commentTxt?.setOnClickListener {
                val menuItem = menuList[adapterPosition]

                it.findNavController().navigate(R.id.action_to_commentRviewFragment, bundleOf(FOOD_ID to menuItem.id,
                    FOOD_NAME to menuItem.title, IS_COMMENT to true))
            }
            //add food for first time
            menuBinding?.addBox?.setOnClickListener {
                selectedPosition = adapterPosition
                menuList[selectedPosition].isCountered = true
                menuList[selectedPosition].count = 1
                menuBinding?.counterFood?.text = "1"
                showCounterBox()
                when {
                    menuList[selectedPosition].isDessert!! -> addDessert(false)
                    menuList[selectedPosition].sideList.isEmpty() -> addDrinks(false)
                    else -> addFood(false)
                }
            }


           //add food or dessert  or drinks to user order
            menuBinding?.plusFood?.setOnClickListener {
                selectedPosition = adapterPosition
                val menuItem = menuList[selectedPosition]
                DataBindingAdapter.setCounter(menuBinding?.counterFood, (++menuItem.count).toString())
                when {
                    menuItem.isDessert!! -> addDessert(true)
                    menuItem.sideList.isEmpty() -> addDrinks(true)
                    else -> addFood(true)
                }
            }

            //remove food or dessert  or drinks from user order
            menuBinding?.minus?.setOnClickListener {
                selectedPosition = adapterPosition
                val menuItem = menuList[selectedPosition]
                if (menuItem.count != 1) {
                  DataBindingAdapter.setCounter(menuBinding?.counterFood, (--menuItem.count).toString())
                    when {
                        menuItem.isDessert!! -> removeDessert(true)
                        menuItem.sideList.isEmpty() -> removeDrink(true)
                        else -> removeFood(true)
                    }
                } else {
                    menuItem.count--
                    menuItem.isCountered = false
                    hideCounterBox()
                    when {
                        menuItem.isDessert!! -> removeDessert(false)
                        menuItem.sideList.isEmpty() -> removeDrink(false)
                        else -> removeFood(false)
                    }
                }
            }
        }

        //get multi view by ViewDataBinding
        fun getListBinding(): ViewDataBinding {
            return DataBindingUtil.getBinding(binding.root)!!
        }

        //add food to user order
        private fun addFood(isCountering: Boolean){
            val menuItem = menuList[selectedPosition]
            if (!isCountering)
               AppManager.order.dishes.add(DishOrder(menuItem.count,menuItem.id,if(menuItem.offProduct=="0") menuItem.price!! else menuItem.offProduct, menuItem.sideList.toMutableList()))
            else
              AppManager.order.dishes.map {
                    if (it.mainID == menuItem.id)
                        it.count = menuItem.count
                }

            funCounter(true)
        }

        //isCountering is for order that added to Order or not
        private fun addDessert(isCountering: Boolean) {
            val menuItem = menuList[selectedPosition]
            if (!isCountering)
                AppManager.order.desserts.add(menuItem)
            else
                AppManager.order.desserts.map {
                    if (it.id == menuItem.id)
                        it.count = menuItem.count
                }

            funCounter(true)
        }

        private fun addDrinks(isCountering: Boolean) {
            val menuItem = menuList[selectedPosition]
            if (!isCountering)
         AppManager.order.drinks.add(menuItem)
            else
        AppManager.order.drinks.map {
                    if (it.id == menuItem.id)
                        it.count = menuItem.count
                }

            funCounter(true)
        }

        //remove food from user order
        private fun removeFood(isCountering: Boolean) {
            val menuItem = menuList[selectedPosition]
            if (!isCountering)
                AppManager.order.dishes.remove(AppManager.order.dishes.find { it.mainID == menuItem.id })
            else
           AppManager.order.dishes.map {
                    if (it.mainID == menuItem.id)
                        it.count = menuItem.count
                }
            funCounter(false)

        }

        private fun removeDessert(isCountering: Boolean) {
            val menuItem = menuList[selectedPosition]
            if (!isCountering)
                ir.nakhodamehran.seefood.Utils.AppManager.order.desserts.remove(ir.nakhodamehran.seefood.Utils.AppManager.order.desserts.find { it.id == menuItem.id })
            else
                ir.nakhodamehran.seefood.Utils.AppManager.order.desserts.map {
                    if (it.id == menuItem.id)
                        it.count = menuItem.count
                }
            funCounter(false)

        }

        private fun removeDrink(isCountering: Boolean) {
            val menuItem = menuList[selectedPosition]
            if (!isCountering)
                ir.nakhodamehran.seefood.Utils.AppManager.order.drinks.remove(ir.nakhodamehran.seefood.Utils.AppManager.order.drinks.find { it.id == menuItem.id })
            else
                ir.nakhodamehran.seefood.Utils.AppManager.order.drinks.map {
                    if (it.id == menuItem.id)
                        it.count = menuItem.count
                }
            funCounter(false)

        }

        private fun hideCounterBox() {
            val startPoint = menuBinding?.counterBox?.width!! / 2
            ViewAnimator.animate(menuBinding?.plusFood).translationX(0f, -startPoint.toFloat()).duration(200).start()
            ViewAnimator.animate(menuBinding?.minus).translationX(0f, startPoint.toFloat()).duration(200).onStop {
                menuBinding?.addBox?.visibility = View.VISIBLE
                menuBinding?.counterBox?.visibility = View.GONE
            }.start()
        }

        private fun showCounterBox() {
            menuBinding?.addBox?.visibility = View.GONE
            menuBinding?.counterBox?.visibility = View.VISIBLE
            val startPoint = menuBinding?.counterBox?.width!! / 2
            ViewAnimator.animate(menuBinding?.plusFood).translationX(-startPoint.toFloat(), 0f).duration(200).start()
            ViewAnimator.animate(menuBinding?.minus).translationX(startPoint.toFloat(), 0f).duration(200).start()
        }


    }


    private fun clearImages(holder: MyViewHolder) {
        holder.itemView.dessertPlace.setImageResource(android.R.color.transparent)
        holder.itemView.dish_img.setImageResource(android.R.color.transparent)
//        holder.itemView.dish_img.setImageResource(R.drawable.plate)
    }

}