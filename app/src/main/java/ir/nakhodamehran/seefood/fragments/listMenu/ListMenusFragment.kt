package ir.nakhodamehran.seefood.fragments.listMenu

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.activities.main.MainActivity
import ir.nakhodamehran.seefood.models.ListMenu
import ir.nakhodamehran.seefood.views.LinearLayoutManagerWithSmoothScroller
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.error_dialog.*
import kotlinx.android.synthetic.main.list_menus_fragment.*
import kotlinx.android.synthetic.main.red_button.*


class  ListMenusFragment : Fragment() {

    //this value for are for tabs
    private var desertStartPosition = -1
    private var drinkStartPosition = -1
    private var mainStartPosition = -1
    //check for user from menu fragment or mainFragment
    private var isFastMenu = false
    private var isUserScrolling = false
    private var totalCounter=0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_menus_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
      StatusBarUtils.setPaddingSmart(activity, topSpac)
        activity?.let {
            (it as MainActivity).binding.content.pageTitle.text=getString(R.string.myOrder)
            it.binding.content.backButton.visibility=View.VISIBLE
        }
        isFastMenu = arguments?.getBoolean(FAST_MENU) ?: false
        configTabs()
        configLists()
        updateTotalCounter()

        confirm_btn.setOnClickListener {
            if(AppManager.order.dishes.isNotEmpty())
            it.findNavController().navigate(R.id.action_to_confirmFragment)
            else {
                val builder = AlertDialog.Builder(activity!!)
                builder.setView(R.layout.error_dialog)
                val dialog = builder.create()
                dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.show()
                dialog.message.text = resources.getString(R.string.err_select_food)
                dialog.negativeButton.toggleVisibility()
                dialog.positiveButton.text = getString(R.string.okey)
                dialog.positiveButton.setOnClickListener {
                    dialog.dismiss()
                }
            }
        }
    }

    // if user order has any record set fot total counter
    private fun updateTotalCounter(){
        totalCounter=0
        AppManager.order.dishes.forEach { totalCounter += it.count }
        AppManager.order.drinks.forEach { totalCounter += it.count }
        AppManager.order.desserts.forEach { totalCounter += it.count }
        totalCounterText.text=  totalCounter.toString()
    }

    //region ConfigList
    private fun configLists() {

        var menuList = mutableListOf<ListMenu>()
        var menuItem: ListMenu

        //Dish
        if(isFastMenu){
            menuList.add(ListMenu(title = getString(R.string.seaFood), isCategoryName = true))

            mainStartPosition = menuList.size - 1

            //sorting list by every item that is special offer
            var fishList= mutableListOf<ListMenu>()
            AppManager.menuCategory?.mainList?.forEach {
                menuItem = ListMenu(it.id,it.title,it.description,price =  it.price,offProduct = it.offProduct
                    , sideList = listOf(KHORMA_ID, LEMON_ID, PEPER_ID, TOMATO_ID, CUCUMBER_ID)
                    , isCategoryName = false
                    , rate = it.rate
                    , time = it.time
                    , isDessert = false)
                fishList.add(menuItem)
            }

            fishList=fishList.sortedWith(compareBy{-it.time}).toMutableList()
            menuList.addAll(fishList)
        }

        //Dessert
        menuList.add(ListMenu(title = getString(R.string.dessert), isCategoryName = true))


        desertStartPosition = menuList.size - 1

        AppManager.menuCategory?.dessertList?.forEach {
            menuItem = ListMenu(it.id,it.title,it.description,
                price =  it.price,
                offProduct = it.offProduct,
                image = it.image,
                isCategoryName = false,
                isDessert = true)

            menuList.add(menuItem)
        }


        //Drink
        menuList.add(ListMenu(title = getString(R.string.drink), isCategoryName = true))

        drinkStartPosition = menuList.size-1

        AppManager.menuCategory?.drinkList?.forEach {
            menuItem = ListMenu(it.id,it.title ,it.description,
                price = it.price,
                offProduct = it.offProduct,
                image = it.image,
                isCategoryName = false)
            menuList.add(menuItem)
        }


        menuList_rv.layoutManager= LinearLayoutManagerWithSmoothScroller(activity)

        menuList_rv.adapter = ListMenuAadapter(menuList){isPlus->
            totalCounter = if (isPlus)
                totalCounter?.plus(1)
            else
                totalCounter?.minus(1)

            totalCounterText.text=totalCounter.toString()
        }

        menuList_rv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val itemPosition= (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                if(isUserScrolling) {

                    //when user back from this fragment no get crash from calling tabs.
                    try {
                        when (itemPosition) {
                          in  drinkStartPosition..menuList.size -> {
                                val tab = if (!isFastMenu)
                                    tabs.getTabAt(1)
                                else
                                    tabs.getTabAt(2)
                                tab?.select()
                            }

                            in desertStartPosition..drinkStartPosition -> {
                                val tab = if (!isFastMenu)
                                    tabs.getTabAt(0)
                                else
                                    tabs.getTabAt(1)
                                tab?.select()
                            }

                            in mainStartPosition..desertStartPosition->{
                                tabs.getTabAt(0)?.select()
                            }
                        }
                    } catch (e: Exception) {
                    }

                }

            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                when (newState) {
                    RecyclerView.SCROLL_STATE_DRAGGING -> isUserScrolling=true
                    RecyclerView.SCROLL_STATE_IDLE -> isUserScrolling=false
                    RecyclerView.SCROLL_STATE_SETTLING -> isUserScrolling=true
                }


            }
        })

    }
    //endregion

    private fun configTabs() {
        Handler().postDelayed({tabs.setCustomFontForTab() },50)
        if (isFastMenu)
            tabs.addTab(tabs.newTab().setText(getString(R.string.seaFood)))

        tabs.addTab(tabs.newTab().setText(getString(R.string.dessert)))
        tabs.addTab(tabs.newTab().setText(getString(R.string.drink)))

        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                val tabPos = tab?.position

                if (!isUserScrolling)
                    when (tabPos) {
                        0 -> {
                            if (isFastMenu)
                                menuList_rv.smoothScrollToPosition(mainStartPosition)
                            else
                                menuList_rv.smoothScrollToPosition(desertStartPosition)

                        }
                        1 -> {
                            if (!isFastMenu)
                                menuList_rv.smoothScrollToPosition(drinkStartPosition)
                            else
                                menuList_rv.smoothScrollToPosition(desertStartPosition)

                        }
                        else -> menuList_rv.smoothScrollToPosition(drinkStartPosition)

                    }


            }
        })
    }
}
