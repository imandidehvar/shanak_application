package ir.nakhodamehran.seefood.fragments.about


import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.StatusBarUtils
import ir.nakhodamehran.seefood.Utils.setCustomFontForTab
import ir.nakhodamehran.seefood.activities.main.MainActivity
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.fragment_about.*
import org.jetbrains.anko.bundleOf


class AboutFragment : Fragment() {

    val aboutList= listOf(SubAboutFragment(),SubAboutFragment(),SubAboutFragment())
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StatusBarUtils.setPaddingSmart(context!!,topSpac)

        activity?.let {
            (it as MainActivity).binding.content.pageTitle.text=getString(R.string.aboutResturant)
            it.binding.content.backButton.visibility=View.VISIBLE
        }

        aboutPager.adapter=AboutAdapter(childFragmentManager)
        tabs.setupWithViewPager(aboutPager)
        //set font for tab texts
        Handler().postDelayed({tabs.setCustomFontForTab() },50)

    }


    inner class AboutAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {

        override fun getPageTitle(position: Int): CharSequence? {
           return when(position){
                0->getString(R.string.aboutUs)
                1->getString(R.string.serComplaint)
                2->getString(R.string.reules)
               else ->""
            }
        }
        override fun getItem(position: Int): Fragment {
            val intro=aboutList[position]
            intro.arguments= bundleOf("aboutType" to position)
            return intro
        }

        override fun getCount(): Int = aboutList.size


    }
}
