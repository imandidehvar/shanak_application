package ir.nakhodamehran.seefood.fragments.address


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.StatusBarUtils
import ir.nakhodamehran.seefood.activities.main.MainActivity
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.fragment_owner_address.*


class OwnerAddressFragment : Fragment() {

    private lateinit var viewModel: AddressViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AddressViewModel::class.java)

    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_owner_address, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StatusBarUtils.setPaddingSmart(activity, topSpac)
        activity?.let {
            (it as MainActivity).binding.content.pageTitle.text=getString(R.string.myAddress)
            it.binding.content.backButton.visibility=View.VISIBLE

        }
        getAddress()

        refreshLayout.setColorSchemeColors(
            ContextCompat.getColor(context!!, android.R.color.holo_orange_light),
            ContextCompat.getColor(context!!, R.color.colorAccent)
        )

        refreshLayout.setOnRefreshListener {
            getAddress()
        }

        addAddressButton.setOnClickListener {
            it.findNavController().navigate(R.id.action_owner_to_addAdressFragment)
        }

    }

    private fun getAddress(){
        refreshLayout.isRefreshing=true
        emptyHolder.visibility=View.GONE
        viewModel.getAddress(refreshLayout,{
            refreshLayout.isRefreshing=false
            val adapter=OwnerAddressAdapter(viewModel,it.toMutableList()){
                emptyHolder.visibility=View.VISIBLE
            }
            address_rv.adapter=adapter
            if(it.isEmpty())
                emptyHolder.visibility=View.VISIBLE
        }){
            getAddress()
            refreshLayout.isRefreshing=false

        }
    }

}
