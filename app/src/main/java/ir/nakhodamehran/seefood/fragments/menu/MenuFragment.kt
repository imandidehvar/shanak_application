package ir.nakhodamehran.seefood.fragments.menu


import android.graphics.Color
import android.graphics.Typeface.BOLD
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import cdflynn.android.library.turn.TurnLayoutManager
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.AppManager
import ir.nakhodamehran.seefood.Utils.afterMeasured
import ir.nakhodamehran.seefood.Utils.toggleVisibility
import ir.nakhodamehran.seefood.activities.main.MainActivity
import ir.nakhodamehran.seefood.models.Dish
import ir.nakhodamehran.seefood.models.DishOrder
import ir.nakhodamehran.seefood.models.Main
import ir.nakhodamehran.seefood.models.Side
import ir.nakhodamehran.seefood.views.MarginDecoration
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.error_dialog.*
import kotlinx.android.synthetic.main.fragment_menu.*
import kotlinx.android.synthetic.main.red_button.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import kotlin.properties.Delegates

class MenuFragment : Fragment() {

    private lateinit var dish: DishOrder
    private lateinit var dishListOrder: MutableList<DishOrder>
    private lateinit var dishList: MutableList<Dish>
    private var secondAdapter: MenuAdapter? = null
    //is empty dish after full dish
    private var activeDish = 0
    //each dish has counter and this will show for that
    private var dishCounter = 0
    //this will show on the red button counter
    private var totalCounter = 0

    //region MainFoodClicked Listener
    var mainFish by Delegates.observable(Main(1, "", "", "", "", icon = "", offProduct = "")) { _, _, newValue ->

        dishList[centerDish].main = newValue.id
        dishes_rv.adapter?.notifyItemChanged(centerDish)
        counterFood.text = 1.toString()
        try {
            if (dishListOrder[centerDish].mainID != newValue.id) {

                totalCounter -= (dishListOrder[centerDish].count - 1)
                totalCounterText.text = totalCounter.toString()

                dishListOrder[centerDish].count = 1
                dishCounter = 1
            }

            dishListOrder[centerDish].mainID = newValue.id

            if (newValue.offProduct != "0")
                dishListOrder[centerDish].price = newValue.offProduct
            else
                dishListOrder[centerDish].price = newValue.price

        } catch (e: Exception) {
            //dish count & dishCounter just for first & no repetitive food will set 1
            if (dishCounter == 0) {
                totalCounterText.text = (++totalCounter).toString()
                dish.count = 1
                dishCounter = 1
            } else if (dish.mainID != newValue.id) {
                totalCounter -= (dish.count - 1)
                totalCounterText.text = totalCounter.toString()
                dish.count = 1
                dishCounter = 1
            }
            dish.mainID = newValue.id
            if (newValue.offProduct != "0") {
                priseText.text = String.format("%,d", newValue.offProduct.toLong())
                dish.price = newValue.offProduct
            } else {
                priseText.text = String.format("%,d", newValue.price.toLong())
                dish.price = newValue.price
            }

        }
    }
    //endregion

    //region SideFoodClicke Listener
    var menuItem by Delegates.observable(Side(1, "", "", "", "")) { _, _, newValue ->
        if (!dishList[centerDish].side.contains(newValue)) {
            dishList[centerDish].side.add(newValue)

            try {
                dishListOrder[centerDish].side.add(newValue.id)
            } catch (e: Exception) {
                dish.side.add(newValue.id)
            }
        } else {
            dishList[centerDish].side.remove(newValue)

            try {
                dishListOrder[centerDish].side.remove(newValue.id)
            } catch (e: Exception) {
                dish.side.remove(newValue.id)
            }
        }
        dishes_rv.adapter?.notifyItemChanged(centerDish)


    }
    //endregion

    companion object {
        //the dish that is current selected or is centered
        var centerDish = 0
        //initial first menus center position
        var centerPrimeryMenu: Int = 0
        var centerSecenderyMenu: Int = 0
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let {
            (it as MainActivity).binding.content.pageTitle.text = getString(R.string.myOrder)
        }
        clearValues()
        configMenu()
        configSecondMenu()
        configDishes()
        configBiases()
        plus_minusFood()
        configConfirmButton()
        setBoldFonts()
        updateDishList()

    }


    private fun clearValues() {
        dishList = mutableListOf(Dish(), Dish(), Dish(), Dish())
        dishListOrder = mutableListOf()
        dishListOrder.addAll(AppManager.order.dishes)

        dish = DishOrder()
        centerDish = 0
        dishCounter = 0
        activeDish = if (AppManager.order.dishes.size != 0) {
            dishCounter = AppManager.order.dishes.first().count
            priseText.text = String.format("%,d", AppManager.order.dishes.first().price.toLong())
            AppManager.order.dishes.size
        } else
            0

        totalCounter = 0
        AppManager.order.dishes.forEach { totalCounter += it.count }
        AppManager.order.drinks.forEach { totalCounter += it.count }
        AppManager.order.desserts.forEach { totalCounter += it.count }
        counterFood.text = dishCounter.toString()
        totalCounterText.text = totalCounter.toString()
    }

    //when user came back to menuFrgment all list must update if order shop has any record
    private fun updateDishList() {
        AppManager.order.dishes.forEachIndexed { index, dishOrder ->
            dishList[index].main = dishOrder.mainID

            for (sideId in dishOrder.side) {
                for (side in AppManager.menuCategory?.sideList!!) {
                    if (sideId == side.id)
                        dishList[index].side.add(side)
                }
            }
        }
        Handler().postDelayed({
            secondAdapter?.resetSecondMenu()
            secondAdapter?.changSideMenu(dishList.first())
        }, 500)

    }

    private fun setBoldFonts() {
        context?.let {
            counterFood.typeface = ResourcesCompat.getFont(it, R.font.iran_sans_bold)
            priseText.typeface = ResourcesCompat.getFont(it, R.font.iran_sans_bold)
        }
    }

    private fun plus_minusFood() {
        plusFood.setOnClickListener {
            try {
                if (dishListOrder[centerDish].mainID != -1) {
                    dishListOrder[centerDish].count = ++dishCounter
                    counterFood.text = dishCounter.toString()

                    totalCounterText.text = (++totalCounter).toString()
                } else
                    context?.toast(getString(R.string.err_select_food))
            } catch (e: Exception) {
                if (dish.mainID != -1) {
                    dish.count = ++dishCounter
                    counterFood.text = dishCounter.toString()

                    totalCounterText.text = (++totalCounter).toString()

                } else
                    context?.toast(getString(R.string.err_select_food))
            }
        }

        minusFood.setOnClickListener {
            try {

                //if have more than one dish
                if (dishListOrder[centerDish].mainID != -1 && dishCounter != 1) {
                    dishListOrder[centerDish].count = --dishCounter
                    counterFood.text = dishCounter.toString()

                    totalCounterText.text = (--totalCounter).toString()

                } else if (dishCounter == 1) {
                    activity?.alert {
                        title = getString(R.string.verify_remove_dish)
                        positiveButton(getString(R.string.yes)) {
                            //remove from orderList
                            dishListOrder.removeAt(centerDish)
                            dishList.removeAt(centerDish)
                            if (centerDish < activeDish)
                                activeDish--
                            dishes_rv.adapter?.notifyItemRemoved(centerDish)
                            dishes_rv.adapter?.notifyItemRangeChanged(centerDish, dishList.size)
                            //add dish to dish list
                            if (centerDish == dishList.size - 1) {
                                dishList.add(Dish())
                                (dishes_rv.adapter as DishAdapter).notifyItemInserted(dishList.size - 1)
                            }

                            //update dishCount
                            dishCounter = try {
                                dishListOrder[centerDish].count
                            } catch (e: Exception) {
                                dish.count
                            }

                            totalCounterText.text = (--totalCounter).toString()

                            counterFood.text = dishCounter.toString()

                            //reset SideMenu
                            secondAdapter?.resetSecondMenu()

                        }
                        negativeButton(getString(R.string.noap)) { it.dismiss() }
                    }?.show()
                } else
                    context?.toast(getString(R.string.err_select_food))
            } catch (e: Exception) {
                if (dish.mainID != -1 && dishCounter != 1) {
                    dish.count = --dishCounter
                    counterFood.text = dishCounter.toString()

                    totalCounterText.text = (--totalCounter).toString()
                } else if (dishCounter == 1) {
                    activity?.alert {
                        title = getString(R.string.verify_remove_dish)
                        positiveButton(getString(R.string.yes)) {
                            dish = DishOrder()
                            dishList.removeAt(centerDish)

                            if (centerDish < activeDish)
                                activeDish--

                            dishes_rv.adapter?.notifyItemRemoved(centerDish)
                            dishes_rv.adapter?.notifyItemRangeChanged(centerDish, dishList.size)

                            if (centerDish == dishList.size - 1) {
                                dishList.add(Dish())
                                (dishes_rv.adapter as DishAdapter).notifyItemInserted(dishList.size - 1)
                            }


                            dishCounter = try {
                                dishListOrder[centerDish].count
                            } catch (e: Exception) {
                                dish.count
                            }

                            totalCounterText.text = (--totalCounter).toString()

                            counterFood.text = dishCounter.toString()


                            secondAdapter?.resetSecondMenu()
                        }
                        negativeButton(getString(R.string.noap)) { it.dismiss() }
                    }?.show()
                } else
                    context?.toast(getString(R.string.err_select_food))
            }
        }
    }

    //set responsive layout for tablets
    private fun configBiases() {
        if (resources.getBoolean(R.bool.is_xlarg_tablet) || resources.getBoolean(R.bool.is_larg_tablet)) {
            (detailText.layoutParams as ConstraintLayout.LayoutParams).verticalBias = 0.8f
            (menu2.layoutParams as ConstraintLayout.LayoutParams).verticalBias = 1.1f
            (dishes_rv.layoutParams as ConstraintLayout.LayoutParams).verticalBias = 0.1f
            (plusFood.layoutParams as ConstraintLayout.LayoutParams).horizontalBias = 0.95f
            (minusFood.layoutParams as ConstraintLayout.LayoutParams).horizontalBias = 1.02f
            (counterFood.layoutParams as ConstraintLayout.LayoutParams).horizontalBias = 0.75f

            (menu2Image.layoutParams as ConstraintLayout.LayoutParams).verticalBias = 0.8f
            (menu2.layoutParams as ConstraintLayout.LayoutParams).verticalBias = 0.97f
        }


    }

    //region MainMenu
    //config food recyclerView
    private fun configMenu() {
        menu1.afterMeasured {
            //set layoutManager for menu1
            layoutManager = TurnLayoutManager(
                activity,              // provide a context
                TurnLayoutManager.Gravity.START,        // from which direction should the list items orbit?
                TurnLayoutManager.Orientation.HORIZONTAL, // Is this a vertical or horizontal scroll?
                1700,               // The radius of the item rotation
                height / 4,                 // Extra offset distance
                false
            )

            val list = AppManager.menuCategory?.mainList

            setDescriptionText(list!![0].title, list[0].coustomDescription)
            menu1.adapter = MenuAdapter(this@MenuFragment, list, null)
            menu1.addItemDecoration(MarginDecoration(true))
            menu1.setOnSnapListener {
                centerPrimeryMenu = it
                setDescriptionText(list[it].title, list[it].coustomDescription)
            }

        }
    }
    //endregion

    //config side recyclerview
    private fun configSecondMenu() {
        menu2.afterMeasured {
            layoutManager = TurnLayoutManager(
                activity,              // provide a context
                TurnLayoutManager.Gravity.START,        // from which direction should the list items orbit?
                TurnLayoutManager.Orientation.HORIZONTAL, // Is this a vertical or horizontal scroll?
                1700,               // The radius of the item rotation
                height / 4,                 // Extra offset distance
                false
            )

            val sideList = AppManager.menuCategory?.sideList
            secondAdapter = MenuAdapter(this@MenuFragment, null, sideList)
            menu2.adapter = secondAdapter

            menu2.addItemDecoration(MarginDecoration(true))
            menu2.setOnSnapListener {
                centerSecenderyMenu = it
                setDescriptionText(sideList!![it].title, sideList[it].description)
            }
        }
    }

    private fun setDescriptionText(title: String, message: String) {
        val spanString = SpannableStringBuilder()
        spanString.append(title).append("\n")
        spanString.append(message)
        spanString.setSpan(ForegroundColorSpan(Color.BLACK), 0, title.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spanString.setSpan(RelativeSizeSpan(1.2f), 0, title.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spanString.setSpan(StyleSpan(BOLD), 0, title.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        detailText.setText(spanString, TextView.BufferType.SPANNABLE)
    }

    //region DishesMenu
    private fun configDishes() {
        dishes_rv.afterMeasured {
            layoutManager = TurnLayoutManager(
                activity,              // provide a context
                TurnLayoutManager.Gravity.START,        // from which direction should the list items orbit?
                TurnLayoutManager.Orientation.HORIZONTAL, // Is this a vertical or horizontal scroll?
                1700,               // The radius of the item rotation
                height / 4,                 // Extra offset distance
                false
            )
        }


        dishes_rv.addItemDecoration(MarginDecoration(false))

        dishes_rv.adapter = DishAdapter(dishList)

        dishes_rv.setOnSnapListener {
            centerDish = it
            //add empty dish to end of dish lists
            if (it == dishList.size - 1) {
                dishList.add(Dish())
                (dishes_rv.adapter as DishAdapter).notifyItemInserted(dishList.size - 1)
            }

            //check previous dish mainID is full or not
            if (validDish()) {
                //check that user just scroll one dish
                when {
                    it == activeDish + 1 -> {
                        if (!dishListOrder.contains(dish)) {
                            activeDish++
                            secondAdapter?.resetSecondMenu()
                            dishListOrder.add(dish)
                            //add new dish that is empty
                            dish = DishOrder()
                        } else {
                            activity?.toast("غذای شما تکراری است.")
                            dishes_rv.smoothScrollToPosition(activeDish)
                        }
                    }
                    //else user scroll more than one dish to empty dishes
                    it > activeDish -> dishes_rv.smoothScrollToPosition(activeDish + 1)
                    it < activeDish || it == activeDish -> {
                        secondAdapter?.changSideMenu(dishList[it])
                    }
                }

            } else {
                dishes_rv.smoothScrollToPosition(activeDish)
                secondAdapter?.resetSecondMenu()
            }

            dishCounter = try {
                priseText.text = String.format("%,d", dishListOrder[it].price.toLong())
                dishListOrder[it].count
            } catch (e: Exception) {
                priseText.text = String.format("%,d", dish.price.toLong())
                dish.count
            }

            counterFood.text = dishCounter.toString()
        }
    }
    //endregion

    //region DishValidation
    private fun validDish(): Boolean {
        return try {
            //when user scroll forward check previous dish is filled or not
            if (activeDish < centerDish)
                dishListOrder[centerDish - 1].mainID != -1
            else
            //when user scroll back to first state of dish check
                dishListOrder[centerDish].mainID != -1
        } catch (r: Exception) {
            dish.mainID != -1
        }
    }
    //endregion


    //region ConfirmButton
    private fun configConfirmButton() {
        confirm_btn.setOnClickListener {
            AppManager.order.dishes.clear()
            AppManager.order.dishes.addAll(dishListOrder)

            if (dish.mainID != -1 && !AppManager.order.dishes.contains(dish))
                AppManager.order.dishes.add(dish)

            if (AppManager.order.dishes.isNotEmpty()) {
                it.findNavController().navigate(R.id.action_to_listMenusFragment)
            } else {
                val builder = AlertDialog.Builder(activity!!)
                builder.setView(R.layout.error_dialog)
                val dialog = builder.create()
                dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.show()
                setUpDialog(dialog)
            }
        }
    }
    //endregion

    private fun setUpDialog(dialog: AlertDialog) {
        dialog.message.text = resources.getString(R.string.err_select_food)
        dialog.negativeButton.toggleVisibility()
        dialog.positiveButton.text = getString(R.string.okey)
        dialog.positiveButton.setOnClickListener {
            dialog.dismiss()
        }
    }


    //get CenterDish in MenuAdapter
    fun getCenterDish(): DishOrder {
        return try {
            dishListOrder[centerDish]
        } catch (e: Exception) {
            dish
        }
    }
}
