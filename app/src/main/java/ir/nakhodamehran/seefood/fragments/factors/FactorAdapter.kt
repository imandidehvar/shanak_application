package ir.nakhodamehran.seefood.fragments.factors

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.AppManager
import ir.nakhodamehran.seefood.Utils.FACTOR
import ir.nakhodamehran.seefood.Utils.FACTORID
import ir.nakhodamehran.seefood.Utils.toggleVisibility
import ir.nakhodamehran.seefood.databinding.FactorItemBinding
import ir.nakhodamehran.seefood.models.Factor
import ir.nakhodamehran.seefood.models.Order
import kotlinx.android.synthetic.main.error_dialog.*
import kotlinx.android.synthetic.main.factor_item.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.bundleOf

class FactorAdapter(val factorList: List<Factor>) : RecyclerView.Adapter<FactorAdapter.MyViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding=FactorItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return MyViewHolder(binding.root)
    }

    override fun getItemCount(): Int =factorList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val binding=DataBindingUtil.getBinding<FactorItemBinding>(holder.itemView)

        val factor=factorList[position]
        binding?.factor=factor

        when(factor.status){

            "onPrepare","onDeliver","onConfirm"->{
                holder.itemView.rippleAnim.visibility=View.VISIBLE
                holder.itemView.rippleAnim.startRippleAnimation()
                holder.itemView.factorFab.setImageResource(R.drawable.ic_chat_support)
                holder.itemView.factorText.text=holder.itemView.context.getString(R.string.supportChat)
            }

            "onPay"->{
                holder.itemView.rippleAnim.visibility=View.VISIBLE
                holder.itemView.rippleAnim.startRippleAnimation()
                holder.itemView.factorFab.setImageResource(R.drawable.ic_payment)
                holder.itemView.factorText.text=holder.itemView.context.getString(R.string.payOrder)
            }

            "onCancel"->{
                holder.itemView.rippleAnim.visibility=View.GONE
                holder.itemView.factorFab.setImageResource(R.drawable.ic_add_black)
                holder.itemView.factorText.text=holder.itemView.context.getString(R.string.repeatOrder)
                 holder.itemView.statusDot.supportBackgroundTintList=ColorStateList.valueOf(Color.RED)
            }
            else->{
                holder.itemView.rippleAnim.visibility=View.GONE
                holder.itemView.factorFab.setImageResource(R.drawable.ic_add_black)
                holder.itemView.factorText.text=holder.itemView.context.getString(R.string.repeatOrder)
            }

        }
    }



    inner class MyViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        init {

            itemView.setOnClickListener {
                val factor=factorList[adapterPosition]

                it.findNavController().navigate(R.id.action_to_factorDetailFragment, bundleOf(FACTOR to factor))
            }

            itemView.repeat_supportFab.setOnClickListener {view->
                val factor=factorList[adapterPosition]

                //check status of factor
                when(factor.status){
                   "onPrepare","onDeliver","onConfirm"->{
                      val builder=AlertDialog.Builder(view.context)
                        builder.setView(R.layout.error_dialog)
                        val dialog=builder.create()
                        dialog.show()
                        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        dialog.negativeButton.toggleVisibility()
                        dialog.title.text=view.context.resources.getString(R.string.support)
                        dialog.message.text=view.context.resources.getString(R.string.supportMessage)
                        dialog.positiveButton.text=view.context.resources.getString(R.string.call)

                        dialog.positiveButton.setOnClickListener {
                            val phone = "025-31880"
                            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
                            it.context.startActivity(intent)}
                    }

                    "onComplete"->{
                        if(AppManager.order.dishes.isNotEmpty())
                            (itemView.context as AppCompatActivity).alert {
                                title=itemView.resources.getString(R.string.err_title)
                                message=itemView.resources.getString(R.string.replaceOrder)
                                positiveButton(itemView.resources.getString(R.string.yes)){
                                    updateOrder(factor)
                                    view.findNavController().navigate(R.id.action_factor_to_confirmOrder)
                                }
                                negativeButton(itemView.resources.getString(R.string.noap)){
                                    it.dismiss()
                                }
                            }.show()
                        else {
                            updateOrder(factor)
                            view.findNavController().navigate(R.id.action_factor_to_confirmOrder)
                        }
                    }

                    else->{
                        if(AppManager.order.dishes.isNotEmpty())
                        (itemView.context as AppCompatActivity).alert {
                            title=itemView.resources.getString(R.string.err_title)
                            message=itemView.resources.getString(R.string.replaceOrder)
                            positiveButton(itemView.resources.getString(R.string.yes)){
                                updateOrder(factor)
                                view.findNavController().navigate(R.id.action_factor_to_confirmOrder, bundleOf(FACTORID to factor.id))
                            }
                            negativeButton(itemView.resources.getString(R.string.noap)){
                                it.dismiss()
                            }
                        }.show()
                        else {
                            updateOrder(factor)
                            view.findNavController().navigate(R.id.action_factor_to_confirmOrder, bundleOf(FACTORID to factor.id))
                        }
                    }

                }
            }
        }
    }

    private fun updateOrder(factor: Factor) {
        AppManager.order = Gson().fromJson<Order>(factor.orderItems, Order::class.java)
        if (AppManager.order.description.isNullOrBlank())
            AppManager.order.description = ""

        if (AppManager.order.discount.isNullOrEmpty())
            AppManager.order.discount = ""
    }
}
