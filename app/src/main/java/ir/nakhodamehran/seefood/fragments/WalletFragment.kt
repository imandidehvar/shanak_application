package ir.nakhodamehran.seefood.fragments


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.AppManager
import ir.nakhodamehran.seefood.Utils.StatusBarUtils
import ir.nakhodamehran.seefood.Utils.retrofitFailError
import ir.nakhodamehran.seefood.Utils.toggleVisibility
import ir.nakhodamehran.seefood.activities.main.MainActivity
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.fragment_wallet.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Pattern


class WalletFragment : Fragment(), AnkoLogger {

    companion object {
        private val MINIMUM_PRISE = 1000
    }

    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }
    private var selectPrice=0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wallet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StatusBarUtils.setPaddingSmart(activity, topSpac)

        activity?.let {
            (it as MainActivity).binding.content.pageTitle.text = getString(R.string.wallet)
        }
        confirmButton()
        configRadioGroups()
        walletPrice.text="${AppManager.profile?.wallet} تومان"
        setInputPrise()
    }

    private fun configRadioGroups(){
        pricesOption.setOnCheckedChangeListener { _, i ->
        when(i){
           R.id.one ->{selectPrice=30000
               priseInput.setText("")
               priseInput.inputType=InputType.TYPE_NULL}
           R.id.tow ->{selectPrice=60000
               priseInput.setText("")
               priseInput.inputType=InputType.TYPE_NULL}

           R.id.three ->{selectPrice=90000
               priseInput.setText("")
               priseInput.inputType=InputType.TYPE_NULL}

           R.id.four -> priseInput.inputType=InputType.TYPE_CLASS_NUMBER
        }
        }
    }


    private fun confirmButton(){



        confirm_btn.setOnClickListener {
            if(selectPrice!=0 && selectPrice >= MINIMUM_PRISE) {
                confirm_progress.toggleVisibility()
                confirmText.toggleVisibility()
                seafoodAPIService.chargeWallet(AppManager.authToken.get()!!, mapOf("amount" to selectPrice.toString()))
                    .enqueue(object : Callback<Map<String, String>> {
                        override fun onFailure(call: Call<Map<String, String>>, t: Throwable) {

                            try {
                                it.retrofitFailError(t) { confirm_btn.performClick() }
                                confirm_progress.toggleVisibility()
                                confirmText.toggleVisibility()
                            } catch (e: Exception) {
                            }
                        }

                        override fun onResponse(
                            call: Call<Map<String, String>>,
                            response: Response<Map<String, String>>
                        ) {
                            if (response.isSuccessful) {
                                val url = response.body()?.get("url")
                                val i = Intent(Intent.ACTION_VIEW)
                                i.data = Uri.parse(url)
                                startActivity(i)
                            } else {
                                activity?.toast(activity?.resources?.getString(R.string.err_support_call)!!)
                            }
                            confirm_progress.toggleVisibility()
                            confirmText.toggleVisibility()
                        }
                    })
            }else if (selectPrice< MINIMUM_PRISE)
                activity?.toast(activity?.resources?.getString(R.string.err_low_price)!!)
            else
                activity?.toast(activity?.resources?.getString(R.string.err_select_price)!!)
        }
    }


    private lateinit var textChangeListener: TextWatcher
    var privieusPrice = ""
    private fun setInputPrise() {
        priseInput.setOnClickListener {
            if(priseInput.inputType==InputType.TYPE_NULL)
                activity?.toast(getString(R.string.anotherPrice))
        }

        textChangeListener = object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(char: CharSequence?, stringLength: Int, p2: Int, p3: Int) {
                    var price = char.toString().split(" ")
                    priseInput?.removeTextChangedListener(textChangeListener)

                    val matcher = Pattern.compile("[^\\\\d]{1,7} تومان").matcher(char.toString())
                    if (!matcher.matches()){
                        when {
                            price[0].isEmpty() -> priseInput?.setText("")

                            price[0].length>7 -> {
                                priseInput?.setText(String.format("%,d", privieusPrice.toLong())+ " تومان")
                                priseInput?.setSelection(privieusPrice.length)
                            }

                            else -> {
                                priseInput?.setText(price[0] + " تومان")
                                priseInput?.setSelection(price[0].length)
                            }
                        }

                    }
                    else{
                        privieusPrice = price[0].replace(",","")
                        var result =price[0].replace(",","")
                        try {
                            selectPrice=result.toInt()
                        } catch (e:Exception) {
                            //for Android Below lollipop ',' character change to '٬' character
                            result =price[0].replace("٬","")
                            selectPrice=result.toInt()
                            privieusPrice = price[0].replace("٬","")

                        }
                        priseInput?.setText(String.format("%,d", result.toLong())+ " تومان")
                        priseInput?.setSelection(result.length)
                    }



                    priseInput?.addTextChangedListener(textChangeListener)

            }

        }
        priseInput?.addTextChangedListener(textChangeListener)
    }

}

