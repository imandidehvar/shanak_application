package ir.nakhodamehran.seefood.fragments.confirms.viewModels

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.pixplicity.easyprefs.library.Prefs
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.AppManager
import ir.nakhodamehran.seefood.Utils.LANGUAGE
import ir.nakhodamehran.seefood.Utils.retrofitFailError
import ir.nakhodamehran.seefood.Utils.toggleVisibility
import ir.nakhodamehran.seefood.models.DishOrder
import ir.nakhodamehran.seefood.models.ListMenu
import ir.nakhodamehran.seefood.models.Order
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import kotlinx.android.synthetic.main.error_dialog.*
import kotlinx.android.synthetic.main.red_button.view.*
import okhttp3.ResponseBody
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

data class ConfirmViewModel(private var _totalPrice: Long?=null,
                            private var _mainFoodTitle:String?=null) : BaseObservable() {

    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }


    var totalPrice: Long?
        @Bindable get() = _totalPrice
        set(value) {
            _totalPrice = value
            notifyPropertyChanged(BR.totalPrice)
        }


    var mainFoodTitle: String?
        @Bindable get() = _mainFoodTitle
        set(value) {
            _mainFoodTitle = value
            notifyPropertyChanged(BR.mainFoodTitle)
        }

    //this is for learn method that used in confirm_item
    fun View.plusClick(order: DishOrder?,listMenu: ListMenu?){
        if(order==null)
            this.context.toast("order Is Null")
    }

    fun getMainFoodTitle(mainId: Int) {
        for ((id, title) in ir.nakhodamehran.seefood.Utils.AppManager.menuCategory!!.mainList!!)
            if (id == mainId)
                mainFoodTitle=title

    }

    fun cheStock(view:View, order:Order, success:()->Unit, retry:()->Unit){
        order.language= Prefs.getString(LANGUAGE,"fa")
        seafoodAPIService.checkStock(order, AppManager.authToken.get()!!).enqueue(object :Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                view.confirm_progress.toggleVisibility()
                view.confirmText.toggleVisibility()
                if(response.isSuccessful)
                    success()
                else{
                    val message=RetrofitClient.parseError(response)
                    val builder = AlertDialog.Builder(view.context)
                    builder.setView(R.layout.error_dialog)
                    val dialog = builder.create()
                    dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    dialog.show()

                    dialog.message.text = message["message"]
                    dialog.negativeButton.toggleVisibility()
                    dialog.positiveButton.text = view.context.getString(R.string.okey)
                    dialog.positiveButton.setOnClickListener {
                        dialog.dismiss()
                    }

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                view.confirm_progress.toggleVisibility()
                view.confirmText.toggleVisibility()
               view.retrofitFailError(t,retry)
            }
        })
    }
}