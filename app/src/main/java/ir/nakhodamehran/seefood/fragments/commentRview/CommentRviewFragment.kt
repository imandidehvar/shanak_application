package ir.nakhodamehran.seefood.fragments.commentRview


import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.activities.main.MainActivity
import ir.nakhodamehran.seefood.fragments.commentRview.comment.CommentFragment
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.fragment_comment_rview.*
import org.jetbrains.anko.bundleOf


class CommentRviewFragment : Fragment() {

    private var title:String?=null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_comment_rview, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StatusBarUtils.setPaddingSmart(context!!,topSpac)

        activity?.let {
            title=arguments?.getString(FOOD_NAME)
            (it as MainActivity).binding.content.pageTitle.text=title
            it.binding.content.backButton.visibility=View.VISIBLE
        }
        commentReviewPager.adapter=FragmentAdapter(childFragmentManager)
        tabs.setupWithViewPager(commentReviewPager)
        Handler().postDelayed({tabs.setCustomFontForTab() },50)

        val isComment= arguments?.getBoolean(IS_COMMENT) ?: false

        if(isComment)
            commentReviewPager.currentItem=1
        else
            commentReviewPager.currentItem=0
    }



    inner class FragmentAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {

        override fun getPageTitle(position: Int): CharSequence? {
            return when(position){
                0->getString(R.string.review)
                1->getString(R.string.comment)
                else ->""
            }
        }
        override fun getItem(position: Int): Fragment {
            val foodId=arguments?.getInt(FOOD_ID)
            var fragment:Fragment
            val arg =bundleOf(
                FOOD_ID to foodId, FOOD_NAME to title)

            return if(position==1) {
                fragment= CommentFragment()
                fragment.arguments= arg
                fragment
            } else {
                fragment= ReviewFragment()
                fragment.arguments= arg
                fragment
            }
            }


        override fun getCount(): Int = 2


    }

}
