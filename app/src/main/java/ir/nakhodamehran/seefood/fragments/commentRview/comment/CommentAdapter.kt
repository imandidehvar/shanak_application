package ir.nakhodamehran.seefood.fragments.commentRview.comment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ir.nakhodamehran.seefood.Utils.displayCercularImage
import ir.nakhodamehran.seefood.databinding.CommentItemBinding
import ir.nakhodamehran.seefood.models.Comment
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import kotlinx.android.synthetic.main.comment_item.view.*


class CommentAdapter(val comentLis:List<Comment>) : RecyclerView.Adapter<CommentAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        val binding=CommentItemBinding.inflate(inflater,parent,false)
        return MyViewHolder(binding.root)
    }

    override fun getItemCount(): Int = comentLis.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       val comment=comentLis[position]
        val binding=DataBindingUtil.getBinding<CommentItemBinding>(holder.itemView)
        binding?.comment=comment

        holder.itemView.context.displayCercularImage(RetrofitClient.BASE_URL+comment.avatar,holder.itemView.profileImage)
    }


    inner class MyViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)
}
