package ir.nakhodamehran.seefood.fragments.confirms


import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.toggleVisibility
import ir.nakhodamehran.seefood.activities.main.MainActivity
import ir.nakhodamehran.seefood.databinding.FragmentConfirmOrderBinding
import ir.nakhodamehran.seefood.fragments.confirms.adapters.ConfirmAdapter
import ir.nakhodamehran.seefood.fragments.confirms.viewModels.ConfirmViewModel
import ir.nakhodamehran.seefood.models.ListMenu
import com.google.android.material.snackbar.Snackbar
import ir.nakhodamehran.seefood.Utils.AppManager
import ir.nakhodamehran.seefood.Utils.FACTORID
import ir.nakhodamehran.seefood.Utils.StatusBarUtils
import ir.nakhodamehran.seefood.views.BottomMarginDecoration
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.fragment_confirm_order.*
import kotlinx.android.synthetic.main.red_button.*
import org.jetbrains.anko.bundleOf


class ConfirmOrderFragment : Fragment() {
    val viewModel: ConfirmViewModel by lazy {
        ConfirmViewModel()
    }

    lateinit var binding: FragmentConfirmOrderBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding= FragmentConfirmOrderBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StatusBarUtils.setPaddingSmart(context!!,topSpac)
        activity?.let {
            (it as MainActivity).binding.content.pageTitle.text=getString(R.string.title_shop)
        }
        confirmText.text=getString(R.string.selectAddress)
        totalCounterText.toggleVisibility()

        if(AppManager.order.dishes.isEmpty() && AppManager.order.desserts.isEmpty())
            showEmptyState()

        configShopList()
        setTotalPrice()

        confirm_btn.setOnClickListener{ view ->
            if(AppManager.order.dishes.isNotEmpty()) {
                confirmText.toggleVisibility()
                confirm_progress.toggleVisibility()
                viewModel.cheStock(confirm_btn, ir.nakhodamehran.seefood.Utils.AppManager.order,{
                    if(arguments!=null){
                        val id= arguments?.getInt(FACTORID)
                        view.findNavController().navigate(R.id.action_to_confirmAdressFragment, bundleOf(FACTORID to id))
                    }
                    else
                        view.findNavController().navigate(R.id.action_to_confirmAdressFragment)

                }){
                    confirm_btn.performClick()
                }

            }else {
                val snak=Snackbar.make(totalCounterText, getString(R.string.invalidOrder), Snackbar.LENGTH_LONG)
                ViewCompat.setLayoutDirection(snak.view, ViewCompat.LAYOUT_DIRECTION_RTL)
                snak.show()
            }
        }
    }

    private fun configShopList(){
        shop_rv.layoutManager=LinearLayoutManager(activity)
        shop_rv.addItemDecoration(BottomMarginDecoration())
        val menuList= mutableListOf<ListMenu>()
        menuList.addAll(AppManager.order.desserts)
        menuList.addAll(AppManager.order.drinks)
        val adapter= ConfirmAdapter(
           AppManager.order.dishes.toMutableList(),
            menuList,
            viewModel
        ) {
            showEmptyState()
        }
        shop_rv.adapter=adapter
    }

    private fun showEmptyState() {
        emptyHolder.toggleVisibility()
        confirmText.text=getString(R.string.order_now)
        confirm_btn.setOnClickListener(null)
        Handler().postDelayed({
            confirm_btn.setOnClickListener {
                it.findNavController().navigate(R.id.action_confirm_to_listMenusFragment)
            }
        },500)

        priceHolder.toggleVisibility()
    }

    private fun setTotalPrice(){
      var totalPrise= 0
        totalPrise+= AppManager.order.desserts.sumBy { it.price!!.toInt() * it.count }
        totalPrise+= AppManager.order.dishes.sumBy { it.price.toInt() * it.count }
        totalPrise+= AppManager.order.drinks.sumBy { it.price!!.toInt() * it.count }
        viewModel.totalPrice=totalPrise.toLong()
        binding.viewModel=viewModel
    }
}
