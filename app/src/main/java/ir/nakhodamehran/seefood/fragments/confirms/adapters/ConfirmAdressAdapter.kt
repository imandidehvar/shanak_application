package ir.nakhodamehran.seefood.fragments.confirms.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.ADDRESS
import ir.nakhodamehran.seefood.Utils.AppManager
import ir.nakhodamehran.seefood.models.Address
import kotlinx.android.synthetic.main.confirm_adress_item.view.*
import org.jetbrains.anko.bundleOf

class ConfirmAdressAdapter(val addressList: List<Address>,val onClick:(Int)->Unit) :RecyclerView.Adapter<ConfirmAdressAdapter.MyViewHolder>(){

    private var mSelected=-1
    var isEnable=true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.confirm_adress_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int =addressList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val address=addressList[position]
        holder.itemView.addressTitle.text="آدرس: ${address.address}"
        holder.itemView.checkbox.isChecked=position==mSelected
        holder.itemView.title.text=address.title

        AppManager.defaultAdressId?.let{
            if(it==address.Id && isEnable) {
                holder.itemView.checkbox.isChecked = true
                AppManager.order.address=address.Id
                onClick(address.regionId)
            }
        }

    }

    inner class MyViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView){
        init {
            val clickListener={view:View->
                if (isEnable) {
                    mSelected=adapterPosition
                    AppManager.defaultAdressId=null
                    AppManager.order.address=addressList[mSelected].Id!!
                    notifyItemRangeChanged(0,addressList.size)
                    onClick(addressList[mSelected].regionId)
                }
            }
            itemView.editButton.setOnClickListener{
                val address=addressList[adapterPosition]
                it.findNavController().navigate(R.id.action_to_addAdressFragment, bundleOf(ADDRESS to address))

            }

            itemView.checkbox.setOnClickListener(clickListener)
            itemView.setOnClickListener(clickListener)
        }
    }


}