package ir.nakhodamehran.seefood.fragments.about

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Transformation
import androidx.recyclerview.widget.RecyclerView
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.models.Rule
import kotlinx.android.synthetic.main.rule_item.view.*

class RuleAdapter(val ruleList:List<Rule.RuleData>) : RecyclerView.Adapter<RuleAdapter.MyViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.rule_item, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int =ruleList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item=ruleList[position]
        holder.itemView.ruleTitle.text=item.title
        holder.itemView.ruleText.text=item.message
        if(!item.isexpanded)
            holder.itemView.lyt_expand.visibility=View.GONE
        else
            holder.itemView.lyt_expand.visibility=View.VISIBLE

        toggleArrow(!item.isexpanded,holder.itemView.bt_expand,false)
        }

    inner class MyViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView){
        init {
            itemView.setOnClickListener {
                val item=ruleList[adapterPosition]
                if(!item.isexpanded) {
                    toggleArrow(false,  itemView.bt_expand)
                    expandAction(itemView.lyt_expand)
                    item.isexpanded=true
                }
                else{
                    toggleArrow(true, itemView.bt_expand)
                    collapse(itemView.lyt_expand)
                    item.isexpanded=false
                }
            }
        }
    }



    private fun expandAction(v: View): Animation {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val targtetHeight = v.measuredHeight

        v.layoutParams.height = 0
        v.visibility = View.VISIBLE
        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                v.layoutParams.height = if (interpolatedTime == 1f)
                    ViewGroup.LayoutParams.WRAP_CONTENT
                else
                    (targtetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        a.duration = (targtetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
        return a
    }

    fun collapse(v: View) {
        val initialHeight = v.measuredHeight

        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
    }


    fun toggleArrow(show: Boolean, view: View): Boolean {
        return toggleArrow(show, view, true)
    }

    fun toggleArrow(show: Boolean, view: View, delay: Boolean): Boolean {
        if (show) {
            view.animate().setDuration((if (delay) 200 else 0).toLong()).rotation(180f)
            return true
        } else {
            view.animate().setDuration((if (delay) 200 else 0).toLong()).rotation(0f)
            return false
        }
    }
}
