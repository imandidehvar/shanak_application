package ir.nakhodamehran.seefood.fragments.menu

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.models.Dish
import ir.nakhodamehran.seefood.models.Main
import ir.nakhodamehran.seefood.models.Side
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import kotlinx.android.synthetic.main.menu_item.view.*
import kotlinx.android.synthetic.main.second_menu_item.view.*
import org.jetbrains.anko.toast

class MenuAdapter(var property: MenuFragment, private val mainList: List<Main>?, private val sideList: List<Side>?) :
    RecyclerView.Adapter<MenuAdapter.MyViewHolder>() {


        var isTomatoChecked = false
        var isPeperChecked = false
        var isLemonChecked = false
        var isCucumberChecked = false
        var isKhormaChecked = false


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = if (mainList != null)
            LayoutInflater.from(parent.context).inflate(R.layout.menu_item, parent, false)
        else
            LayoutInflater.from(parent.context).inflate(R.layout.second_menu_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int = mainList?.size ?: sideList!!.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        mainList?.let {
         val main=it[position]
            Glide.with(holder.itemView.context)
                .load(RetrofitClient.BASE_URL+main.icon)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.itemView.menuImage)

            holder.itemView.menu_txt.text=main.title
        }

        sideList?.let {
            val side=it[position]
            when (side.id) {
                KHORMA_ID->holder.itemView.checkbox.isChecked = isKhormaChecked
                LEMON_ID-> holder.itemView.checkbox.isChecked = isLemonChecked
                CUCUMBER_ID -> holder.itemView.checkbox.isChecked = isCucumberChecked
                TOMATO_ID-> holder.itemView.checkbox.isChecked = isTomatoChecked
                PEPER_ID-> holder.itemView.checkbox.isChecked = isPeperChecked
            }
            Glide.with(holder.itemView.context)
                .load(RetrofitClient.BASE_URL+side.icon)
                .transition(DrawableTransitionOptions.withCrossFade())
                .apply(RequestOptions.centerCropTransform())
                .into(holder.itemView.sideImage)

        }

    }

    fun resetSecondMenu(){
        isTomatoChecked = false
         isPeperChecked = false
         isLemonChecked = false
         isCucumberChecked = false
         isKhormaChecked = false
        notifyDataSetChanged()

    }

    fun changSideMenu(dish: Dish) {
        isTomatoChecked = false
        isPeperChecked = false
        isLemonChecked = false
        isCucumberChecked = false
        isKhormaChecked = false
        dish.side.forEach {
            when (it.id) {
                KHORMA_ID -> isKhormaChecked = true
                LEMON_ID -> isLemonChecked = true
                CUCUMBER_ID -> isCucumberChecked = true
                TOMATO_ID-> isTomatoChecked = true
                PEPER_ID-> isPeperChecked = true
            }
        }
        notifyItemRangeChanged(0, sideList!!.size)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                mainList?.let {
                    if (adapterPosition == MenuFragment.centerPrimeryMenu)
                        property.mainFish = it[adapterPosition]
                }

            }

            sideList?.let { sideList: List<Side> ->
                itemView.clickPlaceHolder.setOnClickListener {

                    if (property.getCenterDish().mainID != -1) {
                            itemView.checkbox.isChecked = !itemView.checkbox.isChecked
                            when (sideList[adapterPosition].id) {
                               KHORMA_ID -> isKhormaChecked = !isKhormaChecked
                                LEMON_ID -> isLemonChecked = !isLemonChecked
                                CUCUMBER_ID-> isCucumberChecked = !isCucumberChecked
                                TOMATO_ID-> isTomatoChecked = !isTomatoChecked
                                PEPER_ID-> isPeperChecked = !isPeperChecked
                            }

                            val test=sideList[adapterPosition]
                            property.menuItem = test
                        } else
                            itemView.context.toast(itemView.context.getString(R.string.err_select_food))
                    }
                }

        }
    }
}