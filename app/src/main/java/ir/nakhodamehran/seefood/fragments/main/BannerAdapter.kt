package ir.nakhodamehran.seefood.fragments.main

import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.asksira.loopingviewpager.LoopingPagerAdapter
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.target.BitmapImageViewTarget
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.GlideApp
import ir.nakhodamehran.seefood.webServisc.RetrofitClient


class BannerAdapter(context: Context, val images:List<String>) :LoopingPagerAdapter<String>(context,images,true){

    override fun inflateView(viewType: Int, container: ViewGroup?, listPosition: Int): View {
        return LayoutInflater.from(context).inflate(R.layout.banner_item, container, false)
    }

    override fun bindView(convertView: View?, listPosition: Int, viewType: Int) {
        val image=convertView?.findViewById<ImageView>(R.id.image)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            GlideApp
                .with(context)
                .load(RetrofitClient.BASE_URL + images[listPosition])
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(image!!)
        } else
        GlideApp.with(context).asBitmap().load(RetrofitClient.BASE_URL + images[listPosition]).centerCrop().into(object : BitmapImageViewTarget(image) {
            override fun setResource(resource: Bitmap?) {
                val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.resources, resource)
                circularBitmapDrawable.cornerRadius=25f
              image!!.setImageDrawable(circularBitmapDrawable)
            }
        })
    }

//    private var inflater:LayoutInflater = LayoutInflater.from(context)
//    override fun isViewFromObject(view: View, `object`: Any): Boolean {
//        return view == `object`
//    }
//
//    override fun getCount(): Int =images.size
//
//
//    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
//        container.removeView(`object` as View)
//    }
//
//    override fun instantiateItem(container: ViewGroup, position: Int): Any {
//        val imageLayout = inflater.inflate(R.layout.banner_item, container, false)!!
//
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            GlideApp
//                .with(context)
//                .load(RetrofitClient.BASE_URL + images[position])
//                .transition(DrawableTransitionOptions.withCrossFade())
//                .into(imageLayout.image)
//        } else
//        GlideApp.with(context).asBitmap().load(RetrofitClient.BASE_URL + images[position]).centerCrop().into(object : BitmapImageViewTarget(imageLayout.image) {
//            override fun setResource(resource: Bitmap?) {
//                val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.resources, resource)
//                circularBitmapDrawable.cornerRadius=25f
//                imageLayout.image.setImageDrawable(circularBitmapDrawable)
//            }
//        })
//
//        container.addView(imageLayout, 0)
//
//        return imageLayout
//    }
}