package ir.nakhodamehran.seefood.fragments.confirms


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.activities.main.MainActivity
import ir.nakhodamehran.seefood.fragments.confirms.adapters.ConfirmAdressAdapter
import ir.nakhodamehran.seefood.models.Address
import ir.nakhodamehran.seefood.views.BottomMarginDecoration
import ir.nakhodamehran.seefood.views.LinearLayoutManagerWithSmoothScroller
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.fragment_confirm_adress.*
import kotlinx.android.synthetic.main.red_button.*
import org.jetbrains.anko.bundleOf
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ConfirmAdressFragment : Fragment() {
    private val addressList = mutableListOf<Address>()
    private var districtPrice = ""
    private var adapter:ConfirmAdressAdapter? =null
    private lateinit var manager:LinearLayoutManagerWithSmoothScroller
    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_confirm_adress, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
      StatusBarUtils.setPaddingSmart(activity, topSpac)

        activity?.let {
            (it as MainActivity).binding.content.pageTitle.text = getString(R.string.selectAddress)
        }

        confirmText.text=getString(R.string.payType)

        totalCounterText.toggleVisibility()
        confirm_btn.setOnClickListener {
            if (districtPrice.isNotEmpty()) {
                if(arguments!=null){
                    val id= arguments?.getInt(FACTORID)
                    it.findNavController().navigate(
                        R.id.action_to_confirmPaymentFragment,
                        bundleOf(DISTRICT_PRICE to districtPrice,FACTORID to id))
                }
                else
                it.findNavController().navigate(
                    R.id.action_to_confirmPaymentFragment,
                    bundleOf(DISTRICT_PRICE to districtPrice))
            }else
                activity?.toast(getString(R.string.addAddressErr))
        }

        getAddress()
        setupRefreash()
        address_rv.addItemDecoration(BottomMarginDecoration())


        addAddressButton.setOnClickListener {
            it.findNavController().navigate(R.id.action_to_addAdressFragment)
        }

        personDeliver_cb.setOnCheckedChangeListener{_,boolean->
            if(boolean)
                disableRecyclerView()
            else
                enableRecyclerView()
        }
    }


    private fun getAddress() {
        refreshLayout.isRefreshing=true
            manager=LinearLayoutManagerWithSmoothScroller(context)
            address_rv.layoutManager=manager

        seafoodAPIService.getAddress(AppManager.authToken.get()!!)
            .enqueue(object : Callback<List<Address>> {
                override fun onResponse(call: Call<List<Address>>, response: Response<List<Address>>) {
                    try {
                        refreshLayout.isRefreshing=false
                        if (response.isSuccessful) {
                            addressList.clear()
                            addressList.addAll(response.body()!!)
                            if (addressList.isNotEmpty()) {
                                 adapter = ConfirmAdressAdapter(
                                        response.body()!!,
                                        ::setDistrictPrice
                                    )
                                address_rv.adapter = adapter
                            } else
                                showEmptyState()
                        }
                    } catch (e: Exception) {
                    }
                }

                override fun onFailure(call: Call<List<Address>>, t: Throwable) {
                    refreshLayout.isRefreshing=false
                    confirm_btn?.retrofitFailError(t) { getAddress() }
                }
            })
    }

    private fun setDistrictPrice(selectedDistrictId: Int) {
      AppManager.districts?.forEach { district ->
            if (district.Id == selectedDistrictId) {
                districtPrice = district.price
                return@forEach
            }
        }
    }

    private fun setupRefreash() {
        refreshLayout.setColorSchemeColors(
            ContextCompat.getColor(context!!, android.R.color.holo_orange_light),
            ContextCompat.getColor(context!!, R.color.colorAccent)
        )

        refreshLayout.setOnRefreshListener {
            getAddress()
        }
    }

    private fun disableRecyclerView(){
        // 1 is that server figure that user ser inPersonDelivery
        AppManager.order.address=1
        districtPrice="0"
        manager.setScrollEnabled(false)
        disableView.toggleVisibility()
        adapter?.isEnable=false
        refreshLayout.isEnabled=false
    }

    private fun enableRecyclerView(){
        districtPrice=""
        manager.setScrollEnabled(true)
        disableView.toggleVisibility()
        adapter?.isEnable=true
        refreshLayout.isEnabled=true
    }
    private fun showEmptyState() {
        emptyHolder.toggleVisibility()
        address_rv.toggleVisibility()
    }
}
