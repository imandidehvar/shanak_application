package ir.nakhodamehran.seefood.fragments.address

import android.view.View
import androidx.lifecycle.ViewModel
import ir.nakhodamehran.seefood.Utils.AppManager
import ir.nakhodamehran.seefood.Utils.retrofitFailError
import ir.nakhodamehran.seefood.models.Address
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class AddressViewModel :ViewModel() {

    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }

    fun getAddress(view: View ,success:(List<Address>)->Unit, retry:()->Unit){
      seafoodAPIService.getAddress(ir.nakhodamehran.seefood.Utils.AppManager.authToken.get()!!).enqueue(object : Callback<List<Address>>{
          override fun onFailure(call: Call<List<Address>>, t: Throwable) {
              view.retrofitFailError(t){retry()}
          }

          override fun onResponse(call: Call<List<Address>>, response: Response<List<Address>>) {
              if(response.isSuccessful)
                  try {
                      success(response.body()!!)
                  }catch (e:Exception){

                  }
          }

      })
    }

    fun addAddress(view: View, address:Address, success:(Int)->Unit, retry:()->Unit){
    seafoodAPIService.addAddress(address, AppManager.authToken.get()!!).enqueue(object : Callback<Map<String,Int>>{

        override fun onResponse(call: Call<Map<String,Int>>, response: Response<Map<String,Int>>) {
            if(response.isSuccessful)
                success(response.body()?.get("address") ?: -1)
        }

        override fun onFailure(call: Call<Map<String,Int>>, t: Throwable) {
            view.retrofitFailError(t){retry()}
        }
    })
    }

    fun editAddress(view: View, address:Address, success:()->Unit, retry:()->Unit){
        seafoodAPIService.editAddress(address,AppManager.authToken.get()!!).enqueue(object : Callback<ResponseBody>{

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if(response.isSuccessful)
                    success()
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                view.retrofitFailError(t){retry()}
            }
        })
    }

    fun deleteAddress(view: View, id:Int, success:()->Unit, retry:()->Unit){
        seafoodAPIService.deleteAddress(id, ir.nakhodamehran.seefood.Utils.AppManager.authToken.get()!!).enqueue(object : Callback<ResponseBody>{

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if(response.isSuccessful)
                    success()
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                view.retrofitFailError(t){retry()}
            }
        })
    }
}