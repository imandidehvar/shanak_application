package ir.nakhodamehran.seefood.fragments.inbox


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.pixplicity.easyprefs.library.Prefs
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.activities.main.MainActivity
import ir.nakhodamehran.seefood.models.Message
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.fragment_inbox.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class InboxFragment : Fragment() {

    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_inbox, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StatusBarUtils.setPaddingSmart(context!!,topSpac)
        activity?.let {
            (it as MainActivity).binding.content.pageTitle.text=getString(R.string.inbox)
            it.binding.content.backButton.visibility=View.VISIBLE
        }

        getList()

        refreshLayout.setColorSchemeColors(
            ContextCompat.getColor(context!!, android.R.color.holo_orange_light),
            ContextCompat.getColor(context!!, R.color.colorAccent)
        )

        refreshLayout.setOnRefreshListener {
            getList()
        }
    }

    private fun getList(){
        refreshLayout.isRefreshing=true
        seafoodAPIService.getMessages(AppManager.authToken.get()!!, mapOf(LANGUAGE to Prefs.getString(LANGUAGE,"fa")))
            .enqueue(object : Callback<Map<String,List<Message>>>{
            override fun onFailure(call: Call<Map<String, List<Message>>>, t: Throwable) {
                try {
                    inboxRV.retrofitFailError(t){
                         getList()
                    }
                    refreshLayout.isRefreshing=false
                } catch (e: Exception) {
                }
            }

            override fun onResponse(
                call: Call<Map<String, List<Message>>>,
                response: Response<Map<String, List<Message>>>
            ) {
                try {

                    if(response.isSuccessful && response.body()!!.getValue("messages").isNotEmpty()) {
                        emptyHolder.visibility=View.GONE
                        inboxRV.adapter = InboxAdapter(response.body()!!.getValue("messages"))
                    }
                    else if(response.body()!!.getValue("messages").isEmpty())
                        emptyHolder.visibility=View.VISIBLE

                    refreshLayout.isRefreshing=false
                } catch (e: Exception) {
                }

            }

        })
    }
}
