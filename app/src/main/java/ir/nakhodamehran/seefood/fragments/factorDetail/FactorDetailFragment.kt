package ir.nakhodamehran.seefood.fragments.factorDetail


import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.google.gson.Gson
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.databinding.FragmentFactorDetailBinding
import ir.nakhodamehran.seefood.models.Factor
import ir.nakhodamehran.seefood.models.Order
import kotlinx.android.synthetic.main.error_dialog.*
import kotlinx.android.synthetic.main.fragment_factor_detail.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.bundleOf


class FactorDetailFragment : Fragment() {

    private lateinit var viewModel:FactorDetailViewModel
    private lateinit var bindind:FragmentFactorDetailBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bindind= FragmentFactorDetailBinding.inflate(inflater,container,false)
        return bindind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StatusBarUtils.setPaddingSmart(activity, topSpac)

        arguments?.let {
            val factor:Factor=it.getParcelable(FACTOR)
            bindind.factor= factor
            viewModel= FactorDetailViewModel(activity!!,factor)
            handleStatus(factor)
            configList()
        }
    }

    private fun configList(){
        factor_rv.adapter=FactorDetailAdapter(viewModel.getMenulist())
        factor_rv.isNestedScrollingEnabled=false
    }

    private fun handleStatus(factor:Factor){
        when(factor.status){
            "onPrepare","onDeliver","onConfirm"->{
                rippleAnim.visibility=View.VISIBLE
                rippleAnim.startRippleAnimation()
                factorFab.setImageResource(R.drawable.ic_chat_support)
                factorText.text=context?.getString(R.string.supportChat)
            }
            "onCancel"->{
                rippleAnim.visibility=View.GONE
                factorText.text=context?.getString(R.string.repeatOrder)
                statusDot.supportBackgroundTintList= ColorStateList.valueOf(Color.RED)
            }
            else->{
                rippleAnim.visibility=View.GONE
                factorFab.setImageResource(R.drawable.ic_add_black)
                factorText.text=context?.getString(R.string.repeatOrder)
            }

        }

        repeat_supportFab.setOnClickListener {view->
            when(factor.status){
                "onPrepare","onDeliver","onConfirm"->{
                    val builder= AlertDialog.Builder(context!!)
                    builder.setView(R.layout.error_dialog)
                    val dialog=builder.create()
                    dialog.show()
                    dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    dialog.negativeButton.toggleVisibility()
                    dialog.title.text=resources.getString(R.string.support)
                    dialog.message.text=resources.getString(R.string.supportMessage)
                    dialog.positiveButton.text=resources.getString(R.string.call)

                    dialog.positiveButton.setOnClickListener {
                        val phone = "025-31880"
                        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
                        it.context.startActivity(intent)}
                }

                else->{
                    if(AppManager.order.dishes.isNotEmpty())
                        (context as AppCompatActivity).alert {
                            title=resources.getString(R.string.err_title)
                            message=resources.getString(R.string.replaceOrder)
                            positiveButton(resources.getString(R.string.yes)){
                                updateOrder(factor)
                                view.findNavController().navigate(R.id.action_detail_to_confirmOrder, bundleOf(FACTORID to factor.id))
                            }
                            negativeButton(resources.getString(R.string.noap)){
                                it.dismiss()
                            }
                        }.show()
                    else {
                        updateOrder(factor)
                        view.findNavController().navigate(R.id.action_detail_to_confirmOrder, bundleOf(FACTORID to factor.id))
                    }
                }

            }
        }
    }


    private fun updateOrder(factor: Factor) {
        AppManager.order = Gson().fromJson<Order>(factor.orderItems, Order::class.java)
        if (AppManager.order.description.isNullOrBlank())
            AppManager.order.description = ""

        if (AppManager.order.discount.isNullOrEmpty())
            AppManager.order.discount = ""
    }
}
