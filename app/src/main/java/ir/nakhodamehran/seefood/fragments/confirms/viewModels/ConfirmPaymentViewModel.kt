package ir.nakhodamehran.seefood.fragments.confirms.viewModels

import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel;
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.retrofitFailError
import ir.nakhodamehran.seefood.models.Order
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.content.Intent
import android.net.Uri
import androidx.navigation.findNavController
import ir.nakhodamehran.seefood.Utils.AppManager


class ConfirmPaymentViewModel : ViewModel() {
    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }
    var isCashChecked=ObservableField(false)
    var discountPrice=ObservableField("0")
    var totalPrice=ObservableField("")
    var discountPercent=ObservableField("")
    var orderPrice=""
    var districtPrice=""



    fun setDiscountCode(view: View, code:String,retry:()->Unit,success:(Boolean,Int)->Unit){
        seafoodAPIService.setDiscount(AppManager.authToken.get()!!, mapOf("code" to code)).enqueue(object: Callback<Map<String,String>>{
            override fun onFailure(call: Call<Map<String, String>>, t: Throwable) {
                view.retrofitFailError(t){retry()}
            }

            override fun onResponse(call: Call<Map<String, String>>, response: Response<Map<String, String>>) {
                if(response.isSuccessful)
                    success(true,response.body()?.get("percent")?.toInt()!!)
                else if(response.code()==400)
                    success(false,-1)
            }
        })
    }


    fun addOrder(view:View, order: Order ,updateId:Int?=null,retry: () -> Unit){
        updateId?.let { order.update=updateId }

        seafoodAPIService.addOrder(order, AppManager.authToken.get()!!).enqueue(object : Callback<Map<String,String>>{
           override fun onFailure(call: Call<Map<String, String>>, t: Throwable) {
               view.retrofitFailError(t){retry()}
           }

           override fun onResponse(call: Call<Map<String, String>>, response: Response<Map<String, String>>) {
               if(response.isSuccessful) {
                   if (response.code() == 200) {
                       val url = response.body()?.get("url")
                       val i = Intent(Intent.ACTION_VIEW)
                       i.data = Uri.parse(url)
                       view.context.startActivity(i)
                   } else if (response.code() == 202)
                       view.context.toast("انجام شد")

                   view.findNavController().popBackStack(R.id.mainFragment,false)

                   AppManager.order=Order()
                   AppManager.isFromPayFragment=true
               }
               }

       })
    }
}
