package ir.nakhodamehran.seefood.fragments.commentRview.comment

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModel
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.AppManager
import ir.nakhodamehran.seefood.Utils.retrofitFailError
import ir.nakhodamehran.seefood.Utils.toggleVisibility
import ir.nakhodamehran.seefood.models.Comment
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import kotlinx.android.synthetic.main.error_dialog.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CommentViewModel:ViewModel(){
    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }

    fun getComments(view: SwipeRefreshLayout, foodId:String,response:(List<Comment>?)->Unit,fail:()->Unit){
        seafoodAPIService.getComments(AppManager.authToken.get()!!, mapOf("id" to foodId)).enqueue(object :
            Callback<List<Comment>> {
            override fun onFailure(call: Call<List<Comment>>, t: Throwable) {
                view.retrofitFailError(t){
                    fail()
                }
                view.isRefreshing=false

            }

            override fun onResponse(call: Call<List<Comment>>, response: Response<List<Comment>>) {
                response(response.body())
                view.isRefreshing=false
            }
        })
    }


    fun setComment(view : View, comment:Map<String,String>, success:()->Unit,retry:(Boolean)->Unit){
        seafoodAPIService.setComments(AppManager.authToken.get()!!,comment).enqueue(object :
            Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                view.retrofitFailError(t){
                    retry(true)
                }
                retry(false)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if(response.isSuccessful){

                    val builder = AlertDialog.Builder(view.context)
                    builder.setView(R.layout.error_dialog)
                    builder.setCancelable(false)
                    val dialog = builder.create()
                    dialog.show()
                    dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    dialog.negativeButton.toggleVisibility()
                    dialog.title.text = view.context.getString(R.string.support)
                    dialog.message.text = view.context.getString(R.string.confirm_send_message)
                    dialog.positiveButton.text = view.context.getString(R.string.okey)
                    dialog.positiveButton.setOnClickListener { dialog.dismiss() }
                    success()
                }
            }

        })
    }

}