package ir.nakhodamehran.seefood.fragments.profile


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.activities.main.MainActivity
import ir.nakhodamehran.seefood.models.Avatar
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import kotlinx.android.synthetic.main.fragment_avatar.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AvatarFragment : Fragment() {

    private var mContext:Context? = null
    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_avatar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StatusBarUtils.setPaddingSmart(context!!,topSpac)
        getAvatarList()



        activity?.displayCercularImage(RetrofitClient.BASE_URL+AppManager.profile?.avatarImage,profileImage)

        handelClickLisener()
    }

    private  var selectedAvatar:Avatar?=null

    private fun getAvatarList(){
        seafoodAPIService.getAvatars().enqueue(object : Callback<List<Avatar>>{
            override fun onFailure(call: Call<List<Avatar>>, t: Throwable) {
                progress.toggleVisibility()
                profileImage.retrofitFailError(t){
                    getAvatarList()
                }
            }

            override fun onResponse(call: Call<List<Avatar>>, response: Response<List<Avatar>>) {
                progress.toggleVisibility()
                if (response.isSuccessful) {
                    val adpter=AvatarAdapter(response.body()!!){
                        activity?.displayCercularImage(RetrofitClient.BASE_URL+it.image,profileImage)
                      selectedAvatar=it
                    }
                    avatar_rv.layoutManager=GridLayoutManager(activity,3)
                    avatar_rv.adapter=adpter
                }
            }

        })
    }


    private fun handelClickLisener(){
        cancel_btn.setOnClickListener { activity?.onBackPressed() }
        confirm_btn.setOnClickListener {
            if(selectedAvatar!=null)
            updateProfile(selectedAvatar!!)
            else
                activity?.onBackPressed()
        }
    }

    private fun updateProfile(selectedAvatar:Avatar){
        val profile=AppManager.profile!!
        profile.avatarId=selectedAvatar.id
        confirm_txt.toggleVisibility()
        confirm_progress.toggleVisibility()
        seafoodAPIService.updateUser(AppManager.authToken.get()!!,profile).enqueue(object: Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                profileImage.retrofitFailError(t){
                    updateProfile(selectedAvatar)
                }
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if(response.isSuccessful){
                    AppManager.profile?.avatarId=selectedAvatar.id
                    AppManager.profile?.avatarImage=selectedAvatar.image
                    confirm_txt.toggleVisibility()
                    confirm_progress.toggleVisibility()
                    activity?.onBackPressed()
                    (mContext as MainActivity).getProfile()
                }

            }

        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext=context
    }

}
