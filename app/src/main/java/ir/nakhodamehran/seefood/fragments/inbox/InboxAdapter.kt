package ir.nakhodamehran.seefood.fragments.inbox

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.DataBindingAdapter
import ir.nakhodamehran.seefood.models.Message
import kotlinx.android.synthetic.main.inbox_item.view.*
import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter

class InboxAdapter(val messagList:List<Message>) :RecyclerView.Adapter<InboxAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.inbox_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int=messagList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val message=messagList[position]
        holder.itemView.htmlTextView.setHtml(message.message,HtmlHttpImageGetter( holder.itemView.htmlTextView))
        DataBindingAdapter.setDate(holder.itemView.dateText,message.date)
        DataBindingAdapter.setTime(holder.itemView.timeText,message.date)
        holder.itemView.title.text=message.title
    }

    inner class MyViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView)
}