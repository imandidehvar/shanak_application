package ir.nakhodamehran.seefood.fragments.confirms

import android.animation.ValueAnimator
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnticipateInterpolator
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.github.florent37.viewanimator.ViewAnimator
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.activities.main.MainActivity
import ir.nakhodamehran.seefood.databinding.ConfirmPaymentFragmentBinding
import ir.nakhodamehran.seefood.fragments.confirms.viewModels.ConfirmPaymentViewModel
import kotlinx.android.synthetic.main.confirm_payment_fragment.*
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.description_dialog.*
import kotlinx.android.synthetic.main.red_button.*
import org.jetbrains.anko.toast


class ConfirmPaymentFragment : Fragment() {

    private var backImageHeight = 0f
    private var infoBoxHeight = 0f
    private var totalPrise = 0

    var isExpanded = false

    private lateinit var viewModel: ConfirmPaymentViewModel
    private lateinit var binding: ConfirmPaymentFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ConfirmPaymentFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StatusBarUtils.setPaddingSmart(activity, topSpac)

        viewModel = ViewModelProviders.of(this).get(ConfirmPaymentViewModel::class.java)
        setPrices()
        binding.viewModel = viewModel

        activity?.let {
            (it as MainActivity).binding.content.pageTitle.text = getString(R.string.payType)
        }
        configExpand()
        configDiscount()
        configPayMethode()
        configConfirm()

        descriptionButton.setOnClickListener {
            val builder = AlertDialog.Builder(activity!!)
            builder.setView(R.layout.description_dialog)
            val dialog = builder.create()
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
            dialog.description.setText(AppManager.order.description)
            dialog.positiveButton.setOnClickListener {
                AppManager.order.description=dialog.description.text.toString()
                dialog.dismiss()
            }
        }
    }

    private fun configConfirm() {
        confirmText.text = getString(R.string.finalConfirm)
        totalCounterText.toggleVisibility()
        confirm_btn.setOnClickListener {
            if (confirm_btn.isEnabled) {
                confirm_btn.isEnabled=false
                confirm_progress.toggleVisibility()
                confirmText.toggleVisibility()
                AppManager.order.desserts.map {
                    with(it) {
                        description = null
                        isCountered = null
                        isDessert = null
                        isCategoryName = null
                    }
                }
                AppManager.order.drinks.map {
                    with(it) {
                        description = null
                        isCountered = null
                        isDessert = null
                        isCategoryName = null
                    }
                }

                val id= arguments?.getInt(FACTORID)
                viewModel.addOrder(it, AppManager.order,id) {
                    confirm_btn.isEnabled=true
                    it.performClick()
                }
            }
        }
    }

    //region Expand
    private fun configExpand() {
        backImage.afterMeasured {
            descriptionButton.translationY= resources.dpToPx(resources.getInteger(R.integer.topmarginDescription).toFloat()).toFloat()

            backImageHeight = this.height.toFloat()
            infoBoxHeight = infoBox.height.toFloat()
            infoBox.toggleVisibility()
            moreFab.setOnClickListener {
                if (!isExpanded) {
                    //animate fabButton
                    ViewAnimator.animate(moreFab)
                        .translationY(
                            resources.dpToPx(-27f).toFloat(),
                            -(infoBoxHeight + resources.dpToPx(27f).toFloat())
                        )
                        .interpolator(AnticipateInterpolator())
                        .duration(1000).start()

                    ViewAnimator.animate(descriptionButton)
                        .translationY(
                            resources.dpToPx(resources.getInteger(R.integer.topmarginDescription).toFloat()).toFloat(),
                            -infoBoxHeight)
                        .interpolator(AnticipateInterpolator())
                        .duration(1000).start()
                    //rotate fabButton
                    ViewAnimator.animate(moreFab).rotation(180f).duration(1000).start()
                    // animate ImageView
                    ViewAnimator.animate(backImage)
                        .waitForHeight()
                        .onStop {
                            infoBox.toggleVisibility()
                        }
                        .interpolator(AnticipateInterpolator())
                        .height(backImageHeight, backImageHeight - infoBoxHeight).duration(1000).start()

                    isExpanded = true
                } else {
                    ViewAnimator.animate(moreFab)
                        .translationY(
                            -(infoBoxHeight + resources.dpToPx(27f).toFloat()),
                            resources.dpToPx(-27f).toFloat()
                        )
                        .interpolator(AnticipateInterpolator())
                        .duration(1000).start()


                    ViewAnimator.animate(descriptionButton)
                        .translationY(
                            -(infoBoxHeight + resources.dpToPx(resources.getInteger(R.integer.topmarginDescription).toFloat()).toFloat()),
                            resources.dpToPx(resources.getInteger(R.integer.topmarginDescription).toFloat()).toFloat()
                        )
                        .interpolator(AnticipateInterpolator())
                        .duration(1000).start()

                    ViewAnimator.animate(moreFab).rotation(0f).duration(1000).start()

                    ViewAnimator.animate(backImage).waitForHeight()
                        .interpolator(AnticipateInterpolator())
                        .onStart {
                            infoBox.toggleVisibility()
                        }
                        .height(backImageHeight - infoBoxHeight, backImageHeight).duration(1000).start()

                    isExpanded = false
                }
            }
        }
    }
    //endregion

    //region Discount
    private fun configDiscount() {
        val mDrawable = ContextCompat.getDrawable(context!!, R.drawable.left_rounded)
        mDrawable?.colorFilter =
            PorterDuffColorFilter(ContextCompat.getColor(context!!, R.color.colorAccent), PorterDuff.Mode.MULTIPLY)
        discountHolder.background = mDrawable

        discountHolder.setOnClickListener {
            activity?.hideSoftKeyboard()
            binding.discountProgressView.visibility = View.VISIBLE
            binding.discountText.visibility = View.GONE
            if (discountInput.text.toString().isNotEmpty())
                viewModel.setDiscountCode(discountHolder, discountInput.text.toString(), {
                    discountHolder.performClick()
                    discountProgressView.visibility = View.INVISIBLE
                    discountText.visibility = View.VISIBLE
                }) { isSuccess, discountPercent ->
                    if (isSuccess)
                        calculateDiscount(discountPercent, discountInput.text.toString())
                    else
                        activity?.toast(getString(R.string.noValid))

                    discountInput.setText("")
                    discountProgressView.visibility = View.INVISIBLE
                    discountText.visibility = View.VISIBLE
                }
            else {
                activity?.toast(getString(R.string.noValid))
                binding.discountProgressView.visibility = View.INVISIBLE
                binding.discountText.visibility = View.VISIBLE

            }

        }
    }
    //endregion

    private fun calculateDiscount(percent: Int, discountCode: String) {
        val discountPrice = (percent * totalPrise) / 100
        viewModel.discountPrice.set(discountPrice.toString())
        viewModel.totalPrice.set(((totalPrise - discountPrice) + viewModel.districtPrice.toInt()).toString())
        viewModel.discountPercent.set("(%$percent)")
        AppManager.order.discount = discountCode
    }

    private fun configPayMethode() {
        if(AppManager.order.payWay=="online")
            setOnline()
        else
            setCash()

        onlinePay.setOnClickListener {
            setOnline()
        }

        cashPay.setOnClickListener {
              setCash()
        }

        creditCheckBox.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                AppManager.order.useWallet = 1

                minusWallet(AppManager.profile?.wallet!!)

            } else {
                AppManager.order.useWallet = 0
                addWallet(AppManager.profile?.wallet!!)

            }

        }
    }

    private fun setCash() {
        viewModel.isCashChecked.set(true)
        creditCheckBox.visibility = View.GONE
        creditInfo.alpha = 0.4f
        AppManager.order.payWay = "cash"
        //do not remove below code
        if (AppManager.order.useWallet == 1) {
            activity?.toast("اعتبار در نقدی استفاده نمیشود.")
            AppManager.order.useWallet = 0
            creditCheckBox.isChecked = false
        }

        cashPay.isChecked = true
    }

    private fun setOnline() {
        viewModel.isCashChecked.set(false)
        creditCheckBox.visibility = View.VISIBLE
        creditInfo.alpha = 1f
        AppManager.order.payWay = "online"
        if (creditCheckBox.isChecked)
            AppManager.order.useWallet = 1

        onlinePay.isChecked = true
    }

    private var point = 0
    private var totalPoint = 0
    private fun minusWallet(price: Int) {

        val totalPrise = viewModel.totalPrice.get()!!.toInt()
        point = if ((totalPrise - price) < 0)
            price - totalPrise
        else
            0
        val animation = ValueAnimator.ofInt(price, point)
        animation.setDuration(500).addUpdateListener {
            DataBindingAdapter.setpriceForText(walletPrice, it.animatedValue.toString())
        }
        animation.start()

        totalPoint = if ((totalPrise - price) < 0)
            0
        else
            totalPrise - price

        val animation2 = ValueAnimator.ofInt(totalPrise, totalPoint)
        animation2.setDuration(500).addUpdateListener {
            viewModel.totalPrice.set(it.animatedValue.toString())
        }
        animation2.start()
    }

    private fun addWallet(price: Int) {
        val totalPrise =
            (this@ConfirmPaymentFragment.totalPrise + viewModel.districtPrice.toInt()) - viewModel.discountPrice.get()!!.toInt()
        val animation = ValueAnimator.ofInt(point, price)
        animation.setDuration(500).addUpdateListener {
            DataBindingAdapter.setpriceForText(walletPrice, it.animatedValue.toString())
        }
        animation.start()


        val animation2 = ValueAnimator.ofInt(totalPoint, totalPrise)
        animation2.setDuration(500).addUpdateListener {
            viewModel.totalPrice.set(it.animatedValue.toString())
        }
        animation2.start()
    }



    private fun setPrices() {
        //Dish Price
        totalPrise += AppManager.order.desserts.sumBy { it.price!!.toInt() * it.count }
        totalPrise += AppManager.order.dishes.sumBy { it.price.toInt() * it.count }
        totalPrise+= AppManager.order.drinks.sumBy { it.price!!.toInt() * it.count }

        viewModel.orderPrice = totalPrise.toString()

        //District Price
        viewModel.districtPrice = arguments?.getString(DISTRICT_PRICE) ?: "0"

        //TotalPrice
        viewModel.totalPrice.set((totalPrise + viewModel.districtPrice.toInt()).toString())

        //WalletPrice
        DataBindingAdapter.setpriceForText(walletPrice, AppManager.profile?.wallet.toString())

    }
}
