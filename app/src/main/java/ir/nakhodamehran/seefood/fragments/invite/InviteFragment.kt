package ir.nakhodamehran.seefood.fragments.invite


import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.AppManager
import ir.nakhodamehran.seefood.Utils.StatusBarUtils
import ir.nakhodamehran.seefood.activities.main.MainActivity
import ir.nakhodamehran.seefood.views.CustomTypefaceSpan
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.fragment_invite.*


class InviteFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.let {
            (it as MainActivity).binding.content.pageTitle.text = getString(R.string.title_invite)
            it.binding.content.backButton.visibility = View.VISIBLE
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_invite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StatusBarUtils.setPaddingSmart(activity, topSpac)

        var giftPrice = String.format("%,d", java.lang.Long.parseLong(AppManager.walletGiftPrice))


        giftPrice= giftPrice.replace("0", "۰")
            .replace("1", "۱")
            .replace("2", "۲")
            .replace("3", "۳")
            .replace("4", "۴")
            .replace("5", "۵")
            .replace("6", "۶")
            .replace("7", "۷")
            .replace("8", "۸")
            .replace("9", "۹")


        configInviteText(giftPrice)


        confirm_btn.setOnClickListener {
            val url = String.format(
                "نصبش کن ,هم ماهی خوشمزه داره,هم %s تومن اعتبار هدیه میده \n",
                giftPrice
            ) + RetrofitClient.BASE_URL + "invite/" + AppManager.profile?.inviteLink
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT, url)
            startActivity(Intent.createChooser(intent, "انتخاب کنید."))
        }

        confirm_btn.setCompoundDrawablesWithIntrinsicBounds(
            AppCompatResources.getDrawable(
                activity!!,
                R.drawable.ic_invite_circle
            ), null, null, null
        )
    }


    private fun configInviteText(giftPrice:String) {

        val message = String.format(getString(R.string.invite_message), giftPrice)
        val span = SpannableString(message)
        span.setSpan(
            CustomTypefaceSpan("", ResourcesCompat.getFont(context!!, R.font.iran_sans_bold)), message.indexOf(giftPrice[0]),
            message.indexOf("۰") + resources.getInteger(R.integer.inviteIndex)+giftPrice.length, Spanned.SPAN_EXCLUSIVE_INCLUSIVE
        )

        span.setSpan(
            ForegroundColorSpan(Color.RED), message.indexOf(giftPrice[0]),
            message.indexOf("۰") + resources.getInteger(R.integer.inviteIndex)+giftPrice.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        span.setSpan(
            RelativeSizeSpan(1.2f), message.indexOf(giftPrice[0]),
            message.indexOf("۰") + resources.getInteger(R.integer.inviteIndex)+giftPrice.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        inviteMessage.setText(span)
    }
}
