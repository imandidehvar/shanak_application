package ir.nakhodamehran.seefood.fragments.commentRview.comment


import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.FOOD_ID
import ir.nakhodamehran.seefood.Utils.toggleVisibility
import kotlinx.android.synthetic.main.add_comment_dialog.*
import kotlinx.android.synthetic.main.fragment_comment.*
import org.jetbrains.anko.toast

class CommentFragment : Fragment() {

    private lateinit var viewModel: CommentViewModel
    private var foodId=0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_comment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProviders.of(this).get(CommentViewModel::class.java)
        foodId = arguments?.getInt(FOOD_ID) ?: 0

        configList()
        configRefresh()

        addComment_btn.setOnClickListener { showAddCommentDialog() }
    }


    private fun configList() {
            refreshLayout.isRefreshing = true
            viewModel.getComments(refreshLayout, foodId.toString(),{
                try {
                    comment_rv.adapter = CommentAdapter(it!!)
                    if(it.isEmpty()){
                        emptyHolder.visibility=View.VISIBLE
                        refreshLayout.visibility=View.GONE
                    }
                } catch (e: Exception) {
                }

            }) {
                configList()
            }
    }

    private fun configRefresh() {
        refreshLayout.setColorSchemeColors(
            ContextCompat.getColor(context!!, android.R.color.holo_orange_light),
            ContextCompat.getColor(context!!, R.color.colorAccent)
        )

        refreshLayout.setOnRefreshListener { configList() }
    }

    private fun showAddCommentDialog(){
        val builder = AlertDialog.Builder(activity!!)
        builder.setView(R.layout.add_comment_dialog)
        val dialog = builder.create()
        dialog.show()
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        //set icon
        var drawable = resources.getDrawable(R.drawable.ic_edit)
        drawable = DrawableCompat.wrap(drawable)
        DrawableCompat.setTint(drawable, resources.getColor(R.color.colorAccent))
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN)
        dialog.messageInput.editText?.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)

        dialog.cancel_btn.setOnClickListener { dialog.dismiss() }

        dialog.confirm_btn.setOnClickListener {
            if(dialog.messageInput.editText?.text!!.isEmpty()){
                activity?.toast(getString(R.string.fillInput))
                return@setOnClickListener
            }

            if(dialog.ratingBar.rating==0f){
                activity?.toast(getString(R.string.submitScore))
                return@setOnClickListener
            }
            dialog.progress.toggleVisibility()
            dialog.confirm_txt.toggleVisibility()

            viewModel.setComment(refreshLayout, mapOf("product_id" to foodId.toString(),"rate" to dialog.ratingBar.rating.toString(),"message" to dialog.messageInput.editText!!.text.toString()),
                {dialog.dismiss()}){
                if(it){
                    dialog.confirm_btn.performClick()
                }
                else {
                    dialog.progress.toggleVisibility()
                    dialog.confirm_txt.toggleVisibility()
                }
            }
        }

    }
}
