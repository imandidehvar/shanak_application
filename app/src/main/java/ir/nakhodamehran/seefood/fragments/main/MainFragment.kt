package ir.nakhodamehran.seefood.fragments.main


import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.asksira.loopingviewpager.LoopingViewPager
import com.pixplicity.easyprefs.library.Prefs
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.*
import ir.nakhodamehran.seefood.activities.main.MainActivity
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import ir.nakhodamehran.seefood.webServisc.SeafoodAPIService
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.fragment_main.*
import org.jetbrains.anko.bundleOf
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainFragment : Fragment() {

    private val seafoodAPIService: SeafoodAPIService by lazy {
        RetrofitClient.SEAFOOD_API_SERVICE
    }
    val imageLists= mutableListOf<String>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StatusBarUtils.setPaddingSmart(this!!.activity, topSpac)

        if(AppManager.isFromPayFragment){
            (activity as MainActivity).getProfile()
        view.findNavController().navigate(R.id.action_to_factorsFragment)
            AppManager.isFromPayFragment=false
            return
        }

        if(imageLists.isEmpty())
            getAds()
        else
            try {
                bannerPager.adapter=BannerAdapter(context!!,imageLists)
                configPageIndicator()
            } catch (e: Exception) {
            }



        activity?.let {
            (it as MainActivity).binding.content.pageTitle.text=getString(R.string.title_main)
            it.binding.content.backButton.visibility=View.GONE

            it.intent.data?.let { uri->
                if(uri.host=="wallet")
                    it.getProfile()
            }

            Handler().postDelayed({
                DataBindingAdapter.setpriceForText(walletPrice,AppManager.profile?.wallet.toString())
            },500)
        }
        customOrder.setOnClickListener {
            it.findNavController().navigate(R.id.action_to_walletFragment)
//            val builder= AlertDialog.Builder(it.context)
//            builder.setView(R.layout.error_dialog)
//            val dialog=builder.create()
//            dialog.show()
//            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            dialog.negativeButton.toggleVisibility()
//            dialog.title.text=it.context.resources.getString(R.string.support)
//            dialog.message.text=it.context.resources.getString(R.string.commingSoon)
//            dialog.positiveButton.text=it.context.resources.getString(R.string.okey)
//            dialog.positiveButton.setOnClickListener { dialog.dismiss()}

        }
        fastOrder.setOnClickListener {
            try {
                it.findNavController().navigate(R.id.action_main_to_listMenus, bundleOf(FAST_MENU to true))
            } catch (e: Exception) {
            }

        }

        factorOrder.setOnClickListener {
            try {
                it.findNavController().navigate(R.id.action_to_factorsFragment)
            } catch (e: Exception) {
            }
        }

        profileButton.setOnClickListener {
            try {
                it.findNavController().navigate(R.id.action_to_profileFragment)
            } catch (e: Exception) {
            }
        }


    }




    private fun getAds(){
        seafoodAPIService.getAds(mapOf("lang" to Prefs.getString(LANGUAGE,"fa"))).enqueue(object :Callback<List<Map<String,String>>>{
            override fun onFailure(call: Call<List<Map<String, String>>>, t: Throwable) {
            }

            override fun onResponse(
                call: Call<List<Map<String, String>>>,
                response: Response<List<Map<String, String>>>) {
                if(response.isSuccessful){
                    response.body()?.forEach {
                        imageLists.add(it.getValue("image"))
                    }

                    try {
                        bannerPager.adapter=BannerAdapter(context!!,imageLists)
                        configPageIndicator()
                    } catch (e: Exception) {
                    }
                }
            }
        })
    }

    fun configPageIndicator() {
        bannerPager.setScrollDurationFactor(8.toDouble())
        pageIndicatorView.count=bannerPager.indicatorCount
        bannerPager.setIndicatorPageChangeListener(object :LoopingViewPager.IndicatorPageChangeListener{
            override fun onIndicatorProgress(selectingPosition: Int, progress: Float) {
                try {
                    if(progress!=1f){
                    bannerPager.pauseAutoScroll()
                    bannerPager.setScrollDurationFactor(1.toDouble())}
                    else{
                            bannerPager.resumeAutoScroll()
                            bannerPager.setScrollDurationFactor(8.toDouble())
                    }
                } catch (e: Exception) {
                }
            }

            override fun onIndicatorPageChange(newIndicatorPosition: Int) {
                try {
                    pageIndicatorView.selection=newIndicatorPosition
                } catch (e: Exception) {
                }

            }

        })
    }


    override fun onResume() {
        super.onResume()
        bannerPager.resumeAutoScroll()
    }

    override fun onStop() {
        super.onStop()
        bannerPager.pauseAutoScroll()
    }

}
