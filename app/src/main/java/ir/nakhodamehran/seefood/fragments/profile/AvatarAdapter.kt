package ir.nakhodamehran.seefood.fragments.profile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.nakhodamehran.seefood.R
import ir.nakhodamehran.seefood.Utils.displayCercularImage
import ir.nakhodamehran.seefood.models.Avatar
import ir.nakhodamehran.seefood.webServisc.RetrofitClient
import kotlinx.android.synthetic.main.avatar_item.view.*

class AvatarAdapter(val avatarList:List<Avatar>,val avatarClick:(Avatar)->Unit) : RecyclerView.Adapter<AvatarAdapter.MyViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.avatar_item, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int =avatarList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        try {
            holder.itemView.context.displayCercularImage(RetrofitClient.BASE_URL+avatarList[position].image,holder.itemView.avatarImage)
        } catch (e: Exception) {
        }

    }


    inner class MyViewHolder(Itemview: View) : RecyclerView.ViewHolder(Itemview){
        init {
            itemView.avatarImage.setOnClickListener {
                val selectedAvatar=avatarList[adapterPosition]
                avatarClick(selectedAvatar)
            }
        }
    }
}
