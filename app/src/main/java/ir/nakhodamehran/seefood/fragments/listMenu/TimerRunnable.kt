package ir.nakhodamehran.seefood.fragments.listMenu

import android.os.Handler
import ir.nakhodamehran.seefood.Utils.setCounterAnimate
import ir.nakhodamehran.seefood.models.ListMenu
import kotlinx.android.synthetic.main.menu_list_item.view.*
import java.util.concurrent.TimeUnit

class TimerRunnable(
    val handler: Handler,
    var holder: ListMenuAadapter.MyViewHolder?,
    var menuItem: ListMenu?,
    var notify: ()->Unit
) : Runnable {

    override fun run() {
        menuItem?.let {

            if (it.time==0){
                it.offProduct="0"
                notify()
                return
            }

            var hour= TimeUnit.SECONDS.toHours(it.time.toLong())
            if(holder?.itemView?.hourText?.text!=hour.toString())
            holder?.itemView?.hourText?.setCounterAnimate(hour.toString())

            var diffMin= menuItem!!.time- TimeUnit.HOURS.toSeconds(hour)
            var minute= TimeUnit.SECONDS.toMinutes(diffMin)
            if(holder?.itemView?.minuteText?.text!=minute.toString())
            holder?.itemView?.minuteText?.setCounterAnimate(minute.toString())

            var seconds= diffMin- TimeUnit.MINUTES.toSeconds(minute)
            holder?.itemView?.secondText?.setCounterAnimate(seconds.toString())
            it.time--
        }
        handler.postDelayed(this,1000)
    }
}
